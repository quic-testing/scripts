**Overview**

There are several QUIC server implementations currently under development and some already running in production. From an attacker's perspective, being able to identify a server implementation can be very valuable, e.g., to launch an implementation specific exploit. Hence, fingerprinting attacks are a part of the attacker's reconnaissance toolkit.

In this repository, are a set of scripts that attempt to fingerprint QUIC server implementations. Being able to fingerprint client implementations is on the roadmap.

**Test Scenarios**

There are multiple tests that are used to collect (behavioural) information about a server. The results from the tests can be used to generate a fingerprint for a server implementation, potentially even a specific release version/git commit.

From a high-level, the current tests are designed around the Connection ID (CID) and New CID aspects of the QUIC protocol (Section 5 from the QUIC Transport Draft). Below is background information on the CID.

***QUIC Connection IDs***

When a client connects to a server for the first time, in addition to using the IP address and ports, the client generates a (pseudo-random) Connection ID (CID) for itself (source CID) as well as for the server (destination CID) it wishes to connect to. “The primary function of a connection ID is to ensure that changes in addressing at lower protocol layers (UDP, IP) don’t cause packets for a QUIC connection to be delivered to the wrong endpoint” [IT20] . These two CIDs are inserted into the QUIC (Long) header in plain-text along with the length of each CID and sent to the server as an Initialpacket. Upon receiving the Initial packet, the server can either generate a new CID for itself or use the CID that the client generated for the server. Regardless, of what it chooses, it then places the CIDs in its response to the client. Once the QUIC handshake is complete, each end-point (i.e., client or server) then only includes the destination CID in the (Short) header. The CID has a maximum length of 20 bytes and follows a variable length encoding scheme.

*** Test Cases***

With some background on the CID, we can now comprehend the test cases and the expected results.

    Case 1
    1.1. The Client connects with scid=1 and dcid=2
    and attempts to send "hello" via a QUIC stream
    and then closes the connection.
    1.2. The client connects with scid=1 and dcid=2
    and attempts to send "hello" and then closes
    the connection.
    Expected Results: Either the second connection attempt succeeds or fails

    Case 2
    2.1. The Client connects with scid=1 and dcid=1
    and attempts to send "hello" via a QUIC stream
    and then closes the connection.
    2.2. The client connects with scid=1 and dcid=1
    and attempts to send "hello" and then closes
    the connection.
    Expected Results: Either the second connection attempt succeeds or fails

    Case 3
    1. The Client connects with scid=1 and dcid=2
    and attempts to send "hello" and then closes
    the connection.
    2. The client connects with scid=1 and dcid=3
    and attempts to send "hello" and then closes
    the connection.
    Expected Results: Either the second connection attempt succeeds or fails


    Case 4
    1. The Client connects with scid=1 and dcid=2
    and attempts to send "hello" and then closes
    the connection.
    2. The client connects with scid=3 and dcid=2
    and attempts to send "hello" and then closes
    the connection.
    Expected Results: Either the second connection attempt succeeds or fails

    Case 5
    1. The Client connects with scid=1 and dcid=2
    and then issues $new_connection_ids$ with cids
    that are *all the same* as the scid (scid=1)
    and then attempts to send "hello" and then
    closes the connection.
    Expected Results: The server crashes, Protocol Violation (error code 10), successfull connection

    Case 6
    1. The Client connects with scid=1 and dcid=2
    and then issues $new_connection_ids$ with cids
    that are *all the same* but different from the
    initial scid and then attempts to send "hello"
    and then closes the connection.
    Expected Results: The server crashes, Protocol Violation (error code 10), successfull connection

    Case 7
    1. The Client connects with scid=1 and dcid=2
    and then issues $new_connection_ids$ with cids
    that are *all the same* as the
    initial dcid and then attempts to send "hello"
    and then closes the connection.
    Expected Results: The server crashes, Protocol Violation (error code 10), successfull connection

In addition to the above CID cases, we can also use the properties and contents of the packets from the server to identify implementations, e.g., the length of the first packet from the server, the Cipher suites/crypto algorithms uses, etc.

More tests can be designed and developed.

**System Design**

There are multiple components to the fingerprinter which is outlined below. An illustration of the system design and workflow is underway.

***Fingerprinting client***
A modified lsquic echo or http client is used to establish a connection with the server (The vanilla lsquic clients always use random CIDs and NCIDs). For each (echo and http) client there are broadly two types of clients:

1) Static SCID and DCID but random NCIDs
2) Static SCID and DCID and static NCIDs

At the moment my approach has been "quick and dirty", so I compiled a few binaries (and if needed modification used bbe to rewrite the binary) to evaluate my ideas. However, in the long term, I'd have to come up with an elegant solution.

To get you started, I've uploaded [the binaries](https://box.hu-berlin.de/f/3dad09192ec34873ae70/?dl=1) that I compiled (amd64 I believe) for my Linux VM on which you could use instead of figuring out what to change to get things working.

The commands to run the http and echo client with debug logs are resp.
`http_client_localhost_scid1_dcid2 -H <IP> -s <PORT> -G <PATH_TO_SAVE_THE_KEY> -L debug -M HEAD -p /`

`echo_client_localhost_scid1_dcid2_newscid2 -H <IP> -s <PORT> -G <PATH_TO_SAVE_THE_KEY> -L debug`

where the default value for `PORT=4433 or 443`. For the http client, the position of the debug option is important to obtain the debug logs.

***Fingerprinting script***
There are 3 fingeprinting scripts: `test-cid.py`, `docker-test-cid.py` and `public-test-cid.py`. The relevant ones for us are `docker-test-cid.py` and `public-test-cid.py`. The docker script is designed to test docker container implementations while the public script is to test publicly reachable servers (as the name suggests). Ignore the test-cid.py script for now.

I believe all the server implementations have a public test server, so you could use them. You can find the links to each of them [on the quic-wg github implementation page](https://github.com/quicwg/base-drafts/wiki/Implementations).

These scripts have the code for the tests described earlier. You can check out the code to get a concrete idea of the steps performed and their sequence.

There are also a number of variables that you will have to set. A more elegant way is to source these variables from a "config file", which can be done at some point.

You will want to pay attention to the interface that `tcpdump` will listen on to capture the packets. I have a VM that I use which I could share with you if you would like so we work from the same reference system.

***Packet capture***
As mentioned, tcpdump is used to capture the packets, but you will need `tshark` also to dissect the packets in the terminal using the scripts. In addition to manually inspecting the packets, you will want to install a new version of `wireshark`, something `>=3.3.0-rc`.

To view the QUIC packets, you will need the keys from the session, so ensure you have the respective key for the packet trace (pcap file), otherwise you will not be able to decode the QUIC packets.

The command to use tshark to view the packets in the terminal is
`tshark -r <case_pcap_file> -o tls.keylog_file:<keys_file> -V`

To view it in wireshark, you will need to follow instructions on how to decode TLS packets. You can also check out the QUIC working group github page for some directions on this.

***Relevent data extraction***

Next, we have the data extraction or collecting the results from the logs of the test cases. This is done via `case1-analysis.sh and filter-logs.sh`.

*case1-analysis is for test case 1 and filter-logs is for case 5, 6 and 7. So make sure results for first server ip len, packets from the server, cid changes and lengths are taken by running case1-analysis otherwise you will be working with incorrect data*

The default path to the data is `../data` from where the script runs.

As the first argument ($1), both scripts take the name of the directory that has data from the tests, e.g., by default this will be a timestamp such as "2020-11-27_12-51-57" and in here are all the results from the cases. Within each case directory are the servers tested and within that are the logs, keys and pcaps. However, if you selectively move the data out, make sure you retain the default directory structre for the analysis scripts.

Within each of these scripts, there are a few steps that have to be taken to extract the relevant information, e.g., the appropriate client type, the resp. key and pcap file.

The commands that extract the results are fairly straightforward. However, manually trying them out on the log will give you a better idea.

The results are saved in a file in the data directory based on the case and server name, so you can distinguish them individually.

Finally, the fingerprint files are copied over to the `analysis` directory.

***Analyzer***
This comprises of the `extract-test-results.sh` script which takes the same argument ($1) as what was given to extract the data.

This script essentially organizes the results into test-specific fingerprint files (.fingeprint) which is easier to read and make observations about the similarities or differences in implementations.

For a more visual approach to this, you could try out the `fingerprint.sh` script on the fingerprint file that was generated by extract-test-results. This will generate a disconnected graph that depicts a disconnected graph where similar implemenations are connected and dissimilar ones are disconnected.

**Classifying The Implementations**
Armed with the tools to collect and filter data, the next step in the process of fingerprinting is classification. Using the .fingerprint files as a basis, which features can be used to classify the differnt implementations?

The FIRST_SERVER_IP_LEN is promising, what about the others, is there a permutation we could use if one does not suffice? It could very well be that FIRST_SERVER_IP_LEN and a couple more suffice for now. We can adapt it as we go along.

To get started, there is already a [dataset](https://gitlab.informatik.hu-berlin.de/quic-testing/analysis) you can start working on. The [raw data](https://gitlab.informatik.hu-berlin.de/quic-testing/data) for that is also present.
