#!/usr/bin/env bash
WD=./
DATADIR=../data/
DATATIMESTAMP=$1
PORT=4433

cd $DATADIR/$DATATIMESTAMP

for case in "case1"
do
echo $case
#for server in "quicly" "aioquic" "quant" "cloudflare" "ngtcp2" "nginx" "mvfst" "pquic" "neqo" "picoquic" "lsquic"
for server in "f5" "quinn" "akamai" "ats" "chromium"
do
echo $server
pwd
ls $case/$server

FINGERPRINT_FILE=$case-$server-fingerprint-$DATATIMESTAMP.out
echo $FINGERPRINT_FILE
case1_1_scid_file=$case/$server/case1_1*scid*.out
case1_2_scid_file=$case/$server/case1_2*scid*.out

PORT=4433
if [[ $server == "akamai" || $server == "quinn" || $server == "google" ]]; then PORT=443; fi

is_qlog=0
result=$(grep "qlog" $case1_1_scid_file -c)
if [[ $result -gt "0" ]]; then is_qlog=$result; fi

is_http=0
result=$(grep -e "HTTP" $case1_1_scid_file -c)
if [[ $result -gt "0" ]]; then is_http=$result; fi
# ASSUMING THE CORRECT CLIENT WAS USED FOR TESTING
# NEED THIS AS SOMETIMES THERE ARE PACKETS BUT NO HTTP RESPONSE
result=$(echo $case1_1_scid_file | grep -c "http")
if [[ $result == "1" ]]; then
is_http=1
fi

if [[ $is_http == "0" ]]; then
# TODO: The out scid*out file for case1-4 needs to have _1 and _2
timestamp=$(ls $case/$server/case1_1*keys | cut -d "-" -f 4-8 | cut -d "." -f 1)
case1_1_keys_file=$case/$server/case1_1-lsquic-$server-$timestamp.keys
case1_1_pcap_file=$case/$server/case1_1-lsquic-$server-$timestamp.pcap
timestamp=$(ls $case/$server/case1_2*keys | cut -d "-" -f 4-8 | cut -d "." -f 1)
case1_2_keys_file=$case/$server/case1_2-lsquic-$server-$timestamp.keys
case1_2_pcap_file=$case/$server/case1_2-lsquic-$server-$timestamp.pcap
else
timestamp=$(ls $case/$server/case1_1*keys | cut -d "-" -f 4-8 | cut -d "." -f 1)
case1_1_keys_file=$case/$server/case1_1-lsquic_http-$server-$timestamp.keys
case1_1_pcap_file=$case/$server/case1_1-lsquic_http-$server-$timestamp.pcap
timestamp=$(ls $case/$server/case1_2*keys | cut -d "-" -f 4-8 | cut -d "." -f 1)
case1_2_keys_file=$case/$server/case1_2-lsquic_http-$server-$timestamp.keys
case1_2_pcap_file=$case/$server/case1_2-lsquic_http-$server-$timestamp.pcap
fi

echo $case1_1_scid_file
echo $case1_1_pcap_file
echo $case1_1_keys_file
echo $case1_2_scid_file
echo $case1_2_pcap_file
echo $case1_2_keys_file

echo "TEST FOR HANDSHAKE_DONE AT THE CLIENT"
result=$(grep -e "handshake: handshake has been confirmed" $case1_2_scid_file -c)
echo "CASE1_2_CONNECT_TO_SERVER=$result, " >> $FINGERPRINT_FILE

echo "TEST FOR PACKETS FROM SERVER"
serverpackets=$(tshark -r $case1_2_pcap_file -o tls.keylog_file:$case1_2_keys_file -V | grep "Source Port: $PORT" -c)
echo "CASE1_2_NO_PACKETS_FROM_SERVER=$serverpackets, " >> $FINGERPRINT_FILE

first_server_ip_len=$(tcpdump -neK -# -M $case1_1_keys_file -r $case1_1_pcap_file src port $PORT | head -n1 | awk {'print $10'} | cut -d ":" -f 1)
echo "FIRST_SERVER_IP_LEN=$first_server_ip_len, " >> $FINGERPRINT_FILE;

serverpackets=$(tshark -r $case1_1_pcap_file -o tls.keylog_file:$case1_1_keys_file -V | grep "Source Port: $PORT" -c)
echo "PACKETS_FROM_SERVER=$serverpackets, " >> $FINGERPRINT_FILE;

result=$(tshark -r $case1_1_pcap_file -o tls.keylog_file:$case1_1_keys_file -V | grep "Source Connection ID: " | cut -d " " -f 8,12,16,20 )
echo $result | tr " " "\n" > source.cids
result=$(grep "0200000000000000" -c source.cids)
if [[ $result == "0" ]]; then
is_cid_changed="1";
echo "IS_CID_CHANGED=1" >> $FINGERPRINT_FILE
fi
if [[ $result -gt "0" ]]; then
is_cid_changed=0;
echo "IS_CID_CHANGED=0" >> $FINGERPRINT_FILE
fi

result=$(tshark -r $case1_1_pcap_file -V | grep "Source Port: $PORT" -A 22 | grep "Source Connection ID Length: " -m 1 | cut -d " " -f 9)
echo "CID_LENGTH="$result >> $FINGERPRINT_FILE


done
done
