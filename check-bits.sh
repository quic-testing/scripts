#!/usr/bin/env bash
TX_PATH="/root/QUIC/Experiments/data/tx/"
RX_PATH="/root/QUIC/Experiments/data/rx/"

echo "**** TX ****"
echo "** Sent 1 **"
for i in $TX_PATH*out; do  grep -e "handshake: handshake has been confirmed" $i -l; done | sort -V | cut -d "/" -f 7
echo ""
echo ""
echo "**** RX **** "
echo "** Got 0 **"
for i in $RX_PATH*out; do  grep -e "handshake: handshake has been confirmed" $i -l; done | sort -V | cut -d "/" -f 7
