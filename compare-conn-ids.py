import json, sys, pprint


analysispath = "/Users/hashkash-imac/Documents/HUB/my_work/QUIC/Experiments/analysis/Chromium/"
analysisfile = "client-server-connid-map.json"
comparisonfile = "connid-comparison.json"

data = json.loads(open(analysispath + analysisfile).read())

# pprint.pprint(data)

resultdict = {}

for cw, sw in zip(data["client"], data["server"]):
    same_connid = False
    # pprint.pprint(data["client"][cw])
    # pprint.pprint(data["server"][sw])
    for id in data["client"][cw]:
        # print data["client"][cw][id][-1], data["server"][sw][id]
        if data["server"][sw] is None:
            resultdict[data["client"][cw][id][0]] = None
            continue
        if id not in data["server"][sw]:
            resultdict[data["client"][cw][id][0]] = None
            continue
        if data["client"][cw][id][-1] == data["server"][sw][id]:
            resultdict[data["client"][cw][id][0]] = True
        else:
            resultdict[data["client"][cw][id][0]] = False
# pprint.pprint(resultdict)
f = open(analysispath + comparisonfile, 'w')
f.write(json.dumps(resultdict))
for i in resultdict:
    if resultdict[i] is False:
        print i

