#!/usr/bin/env bash
LSQUIC_PATH="/root/QUIC/Code/lsquic/build/"
RX_PATH="/root/QUIC/Experiments/data/rx/"

frame=""

for i in `ls $RX_PATH*out | sort -V`;
do
result=0
result=$(grep -e "handshake: handshake has been confirmed" $i -c)
if [[ $result == "0" ]]; then
#echo "Got 0"
frame+="1"
else
frame+="0"
fi
done
echo $frame
