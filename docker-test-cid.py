"""
Created on Mon Sep 7 2020

@author: Kashyap Thimmaraju
@email: k.thimmaraju@informatik.hu-berlin.de
"""

from explib import *

from datetime import datetime
import pprint

logtimestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
imacinterface = "lo0"
quicvminterface = "docker0"
capture_interface = quicvminterface
server_interface = "lo0"
imacpath = "/Users/hashkash-imac/Documents/HUB/my_work/QUIC/"
quicvmpath = "/root/QUIC/"
scriptPath = quicvmpath + "Experiments/scripts/"
dut_server = cnx_quicvm
dut_client = cnx_quicvm

##### quic servers #####
servers = ["lsquic", "quicly", "aioquic", "quant", "mvfst"]
servers = ["lsquic", "quicly", "quant", "mvfst", "aioquic"]
servers = ["lsquic", "quicly", "quant"]
servers = ["aioquic"]
servers = ["quant"]
servers = ["quicly", "aioquic", "quant", "cloudflare", "ngtcp2", "nginx", "mvfst", "pquic", "neqo", "picoquic"]
servers = ["quicly"]
# servers = ["mvfst", "pquic", "neqo", "picoquic"]
# servers = ["picoquic"]
# servers = ["msquic"]
# servers = ["mvfst"]
# servers = ["lsquic"]
# servers = ["lsquic"]

clients = ["lsquic", "lsquic_http"]
clients = ["lsquic"]

##### quic server commands #####
quicly_server = ""
lsquic_server = ""
aioquic_server = ""
quant_server = ""
mvfst_server = ""

QUIC_PATH = quicvmpath + "Code/"

QUICLY_SERVER_PATH = QUIC_PATH + "quicly/"
LSQUIC_SERVER_PATH = QUIC_PATH + "litespeed/lsquic/"
QUANT_SERVER_PATH = QUIC_PATH + "quant/Debug/"
MVFST_SERVER_PATH = QUIC_PATH + "mvfst/_build/build/quic/samples/"
AIOQUIC_SERVER_PATH = QUIC_PATH + "aioquic/"


LSQUIC_CLIENT_PATH = QUIC_PATH + "lsquic/build/"

CASE1 = "case1"
CASE2 = "case2"
CASE3 = "case3"
CASE4 = "case4"
CASE1_1 = "case1_1"
CASE1_2 = "case1_2"
CASE2_1 = "case2_1"
CASE2_2 = "case2_2"
CASE3_1 = "case3_1"
CASE3_2 = "case3_2"
CASE4_1 = "case4_1"
CASE4_2 = "case4_2"
CASE5 = "case5"
CASE6 = "case6"
CASE7 = "case7"


def cleanup(servers, clients):
    print "cleanup()"
    for server in servers:
        stopserver(server)
    for client in clients:
        stopclient(client)
    stopPacketCapture(dut_client)
    print "cleanup done!"


def getservercommand(server, timestamp, datapath, case):
    print "getservercommand()"
    '''
    **** ngtcp2 docker image ****
    sudo docker run -p4433:4433/udp -t -i --entrypoint bash ngtcp2/ngtcp2-interop
    mkdir /www
    server 172.17.0.2 4433 /etc/ngtcp2/server.key /etc/ngtcp2/server.crt -s -d /www --qlog-dir /tmp
    
    '''
    # TODO: Consider running each server on a dedicated port instead of all of them using 4433
    if server == "quicly":
        servercommand = "bash -c \'" + \
                        "docker run -p4433:4433/udp quicly-qfp \'"
        return servercommand
    elif server == "lsquic":
        '''We use the quicly private keys and certs for lsquic also'''
        servercommand = "bash -c \'" + \
                        "docker run -p4433:4433/udp lsquic-qfp \'"
        return servercommand
    elif server == "aioquic":
        servercommand = "bash -c \'" + \
                        "docker run -p4433:4433/udp aioquic-qfp \'"
        return servercommand
    elif server == "quant":
        servercommand = "bash -c \'" + \
                        "docker run -p4433:4433/udp ntap/quant \'"
        return servercommand
    elif server == "mvfst":
        '''
        when using the docker image lnicco/mvfst-qns
        sudo docker run -p4433:4433/udp -t -i --entrypoint bash lnicco/mvfst-qns
        /proxygen/proxygen/_build/proxygen/httpserver/hq --mode=server --port=4433 --static_root=/www --logdir=/tmp --qlogger_path=/tmp --host=172.17.0.2 --congestion=bbr --pacing=true --v=2
        '''
        servercommand = "bash -c \'" + \
                        "docker run -p4433:4433/udp -t -i --entrypoint /run_endpoint.sh mvfst-qfp \'"
        return servercommand
    elif server == "cloudflare":
        servercommand = "bash -c \'" + \
                        "docker run -p4433:4433/udp cloudflare/quiche-qns \'"
        return servercommand
    elif server == "ngtcp2":
        servercommand = ""
        servercommand = "bash -c \'" + \
                        "docker run -p4433:4433/udp ngtcp2-qfp \'"
        return servercommand
    elif server == "nginx":
        servercommand = "bash -c \'" + \
                        "docker run -p4433:4433/udp nginx-qfp \'"
        return servercommand
    elif server == "picoquic":
        servercommand = "bash -c \'" + \
                        "docker run -p4433:4433/udp picoquic-qfp \'"
        return servercommand
    elif server == "quic-go":
        servercommand = ""
        return servercommand
    elif server == "neqo":
        servercommand = "bash -c \'" + \
                        "docker run -p4433:4433/udp neqoquic/neqo-qns \'"
        return servercommand
    elif server == "pquic":
        servercommand = "bash -c \'" + \
                        "docker run -p4433:4433/udp pquic-qfp \'"
        return servercommand
    elif server == "msquic":
        servercommand = "bash -c \'" + \
                        "docker run -p4433:4433/udp msquic \'"
        return servercommand
    else:
        return "noServer"


def startserver(server, timestamp="now", datapath="/tmp/", case="case"):
    print "startserver()"
    if server == "quic-go":
        print "Wont' start server as it's running manually."
        return
    servercommand = getservercommand(server, timestamp, datapath, case)
    print "Going to start server: " + server + ", on " + dut_server[0] + " on interface:" + server_interface
    servercommand = "screen -dmSL serverSession " + servercommand
    ssh = SSHConnect(cnx=dut_server)
    [output, error] = RunCommand(ssh, servercommand)
    if server == "picoquic":
        print "Need to sleep for ~1 minute, as the code needs to build before it starts..."
        time.sleep(60)
    ssh.close()
    return error


def stopserver(server):
    print "stopserver()"
    if server == "quic-go":
        print "Won't stop server as it's running manually."
        return
    print "Going to stop server: " + server
    ssh = SSHConnect(cnx=dut_server)
    # RunCommand(ssh, "screen -ls | grep Detached | "
    #                 "grep " + server + "|"
    #                 "cut -d . -f1 | awk '{print $1}' |"
    #                 "xargs kill")

    RunCommand(ssh, "docker ps | grep " + server + " | awk {'print $1'} | xargs docker kill")

    # if server == "quicly":
    #     RunCommand(ssh, "pkill cli")
    # elif server == "lsquic":
    #     RunCommand(ssh, "pkill echo_server")
    # elif server == "aioquic":
    #     # RunCommand(ssh, "screen -ls | grep Detached | "
    #     #             "grep serverSession " + " |"
    #     #             "cut -d . -f1 | awk '{print $1}' |"
    #     #             "xargs kill")
    #     RunCommand(ssh, "pkill python3")
    # elif server == "mvfst":
    #     RunCommand(ssh, "pkill echo")
    # elif server == "quant":
    #     RunCommand(ssh, "docker ps | grep " + server + " | awk {'print $1'} | xargs docker kill")
    print "server stopped successfully"
    ssh.close()


def getclientcommand(client, timestamp, datapath, case, attempt=1):
    print "getclientcommand()"
    if client == "lsquic":
        clientpath = LSQUIC_CLIENT_PATH
        #servercommand = #"\'" + \

        if case == CASE1 or case == CASE1_1 or case == CASE1_2 or case == CASE3_1 or case == CASE4_1:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "echo_client_localhost_scid1_dcid2 -H " + \
                            "172.17.0.2 -s 4433 -G " + \
                            datapath + " -L " + \
                            "debug 2>" + datapath + \
                            case + "-" + client + "-scid1_dcid2-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE2_1 or case == CASE2_2:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "echo_client_localhost_scid1_dcid1 -H " + \
                            "172.17.0.2 -s 4433 -G " + \
                            datapath + " -L " + \
                            "debug 2>" + datapath + \
                            case + "-" + client + "-scid1_dcid1-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE3_2:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "echo_client_localhost_scid1_dcid3 -H " + \
                            "172.17.0.2 -s 4433 -G " + \
                            datapath + " -L " + \
                            "debug 2>" + datapath + \
                            case + "-" + client + "-scid1_dcid3-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE4_2:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "echo_client_localhost_scid3_dcid2 -H " + \
                            "172.17.0.2 -s 4433 -G " + \
                            datapath + " -L " + \
                            "debug 2>" + datapath + \
                            case + "-" + client + "-scid3_dcid2-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE5:
            # "127.0.0.1 -s 4433 -G " + \
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "echo_client_localhost_scid1_dcid2_newscid1 -H " + \
                            "172.17.0.2 -s 4433 -G " + \
                            datapath + " -L " +\
                            "debug 2>" + datapath + \
                            case + "-" + client + "-scid1_dcid2_newscid1-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE6:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "echo_client_localhost_scid1_dcid2_newscid3 -H " + \
                            "172.17.0.2 -s 4433 -G " + \
                            datapath + " -L " +\
                            "debug 2>" + datapath + \
                            case + "-" + client + "-scid1_dcid2_newscid3-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE7:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "echo_client_localhost_scid1_dcid2_newscid2 -H " + \
                            "172.17.0.2 -s 4433 -G " + \
                            datapath + " -L " +\
                            "debug 2>" + datapath + \
                            case + "-" + client + "-scid1_dcid2_newscid2-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
    elif client == "lsquic_http":
        clientpath = LSQUIC_CLIENT_PATH
        # ./http_client -4 -H 172.17.0.2 -s 4433 -G ./quic-keys/  -p / -M HEAD
        if case == CASE1 or case == CASE1_1 or case == CASE1_2 or case == CASE3_1 or case == CASE4_1:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "http_client_localhost_scid1_dcid2 -H " + \
                            "172.17.0.2 -s 4433 -G " + \
                            datapath + " -L debug " + \
                            "-M HEAD -p / " + \
                            "2&>" + datapath + \
                            case + "-" + client + "-scid1_dcid2-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE2_1 or case == CASE2_2:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "http_client_localhost_scid1_dcid1 -H " + \
                            "172.17.0.2 -s 4433 -G " + \
                            datapath + " -L debug " + \
                            "-M HEAD -p / " + \
                            "2&>" + datapath + \
                            case + "-" + client + "-scid1_dcid1-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE3_2:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "http_client_localhost_scid1_dcid3 -H " + \
                            "172.17.0.2 -s 4433 -G " + \
                            datapath + " -L debug " + \
                            "-M HEAD -p / " + \
                            "2&>" + datapath + \
                            case + "-" + client + "-scid1_dcid3-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE4_2:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "http_client_localhost_scid3_dcid2 -H " + \
                            "172.17.0.2 -s 4433 -G " + \
                            datapath + " -L debug " + \
                            "-M HEAD -p / " + \
                            "2&>" + datapath + \
                            case + "-" + client + "-scid3_dcid2-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE5:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "http_client_localhost_scid1_dcid2_newscid1 -H " + \
                            "172.17.0.2 -s 4433 -G " + \
                            datapath + " -L debug " + \
                            "-M HEAD -p / " + \
                            "2&>" + datapath + \
                            case + "-" + client + "-scid1_dcid2_newscid1-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE6:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "http_client_localhost_scid1_dcid2_newscid3 -H " + \
                            "172.17.0.2 -s 4433 -G " + \
                            datapath + " -L debug " + \
                            "-M HEAD -p / " + \
                            "2&>" + datapath + \
                            case + "-" + client + "-scid1_dcid2_newscid3-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE7:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "http_client_localhost_scid1_dcid2_newscid2 -H " + \
                            "172.17.0.2 -s 4433 -G " + \
                            datapath + " -L debug " + \
                            "-M HEAD -p / " + \
                            "2&>" + datapath + \
                            case + "-" + client + "-scid1_dcid2_newscid2-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand


def startclient(client, timestamp, datapath, case, attempt=1):
    print "startclient()"
    clientcommand = getclientcommand(client, timestamp, datapath, case, attempt)
    print "Going to start client on " + dut_client[0]
    clientcommand = "screen -dmSL clientSession " + clientcommand
    ssh = SSHConnect(cnx=dut_client)
    [output, error] = RunCommand(ssh, clientcommand)
    ssh.close()
    return error



def stopclient(client):
    print "stopclient()"
    print "Going to stop client: " + client
    ssh = SSHConnect(cnx=dut_client)
    # RunCommand(ssh, "screen -ls | grep Detached | "
    #                 "grep " + server + "|"
    #                 "cut -d . -f1 | awk '{print $1}' |"
    #                 "xargs kill")
    if client == "lsquic":
        # Need to stop the client twice if the echo connection is successful.
        RunCommand(ssh, "pkill echo_client")
        # RunCommand(ssh, "screen -S clientSession -X stuff $'\003'")
    elif client == "lsquic_http":
        RunCommand(ssh, "pkill http_client")
        # RunCommand(ssh, "screen -S clientSession -X stuff $'\003'")
    print "client stopped successfully"
    ssh.close()


def saveclientkeys(client, server, timestamp, datapath, case):
    print "saveclientkeys()"
    print "Going to save the client's key."
    ssh = SSHConnect(cnx=dut_client)
    key = ""
    if case != CASE4_2:
        key = "0100000000000000.keys"
    elif case == CASE4_2:
        key = "0300000000000000.keys"
    RunCommand(ssh, "mv " + datapath + key + " " +
               datapath + case + "-" + client + "-" + server + "-" + timestamp + ".keys")
    ssh.close()


def runtests(testcases, server):
    print "runtests()"
    result = dict.fromkeys(range(0, 5))
    for testcase in testcases:
        if testcase == "case1":
            result[0] = case1(server)
        elif testcase == "case2":
            case2(server)
        elif testcase == "case3":
            case3(server)
        elif testcase == "case4":
            case4(server)
        elif testcase == "case5":
            case5(server)
        elif testcase == "case6":
            case6(server)
    return result


def case1(client, server):
    print "case1"
    '''
    Case 1
    1.1. The Client connects with scid=1 and dcid=2
    and attempts to send "hello" via a QUIC stream
    and then closes the connection.
    1.2. The client connects with scid=1 and dcid=2
    and attempts to send "hello" and then closes
    the connection.
    '''

    #### Run the appropriate server from the below list
    '''
    The server list could change over time
    '''
    print "**** Clean up everything before starting ****"
    cleanup(servers, clients)
    print "\n\n"
    print "**** CASE 1 START ****"
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    datapath = prepareDataPath(dut_client, logtimestamp, CASE1, server)

    # First start the server
    startserver(server, timestamp, datapath, CASE1)
    print "**** STARTED THE SERVER *****\n\n"
    # exit()

    # Then start packet capture
    startPacketCapture(dut_client, capture_interface,
                       CASE1_1 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port 4433 or icmp\'"
                       )
    print "**** STARTED PACKET CAPTURE *****\n\n"

    print "Sleep 2s before starting..."
    time.sleep(2)

    # Start the client
    print "Now to start the client"
    # timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startclient(client, timestamp, datapath, CASE1_1)
    print "**** STARTED THE CLIENT'S FIRST ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client and then start it again.
    stopclient(client)
    saveclientkeys(client, server, timestamp, datapath, CASE1_1)
    stopPacketCapture(dut_client)
    exit()

    print "Sleep 10s and then start the client again..."
    time.sleep(10)

    print "Start the packet capture again..."
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startPacketCapture(dut_client, capture_interface,
                       CASE1_2 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port 4433 or icmp\'"
                       )
    print "Sleep 2s and then start the client..."
    time.sleep(2)
    startclient(client, timestamp, datapath, CASE1_2)
    print "**** STARTED THE CLIENT'S SECOND ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client, server and packet capture .
    stopclient(client)
    saveclientkeys(client, server, timestamp, datapath, CASE1_2)
    stopserver(server)

    time.sleep(2)
    stopPacketCapture(dut_client)
    print "**** STOPPED THE CLIENT, SAVED THE KEYS, STOPPED THE SERVER AND PACKET CAPTURE ****\n\n"
    print "**** CASE 1 END ****"


def case2(client, server):
    print "case2"
    '''
    Case 2
    2.1. The Client connects with scid=1 and dcid=1
    and attempts to send "hello" via a QUIC stream
    and then closes the connection.
    2.2. The client connects with scid=1 and dcid=1
    and attempts to send "hello" and then closes
    the connection.
    '''

    #### Run the appropriate server from the below list
    '''
    The server list could change over time
    '''
    print "**** Clean up everything before starting ****"
    cleanup(servers, clients)
    print "\n\n"
    print "**** CASE 2 START ****"
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    datapath = prepareDataPath(dut_client, logtimestamp, CASE2, server)

    # First start the server
    startserver(server, timestamp, datapath, CASE2)
    print "**** STARTED THE SERVER *****\n\n"
    # exit()

    # Then start packet capture
    startPacketCapture(dut_client, capture_interface,
                       CASE2_1 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port 4433 or icmp\'"
                       )
    print "**** STARTED PACKET CAPTURE *****\n\n"

    print "Sleep 2s before starting..."
    time.sleep(2)

    # Start the client
    print "Now to start the client"
    # timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startclient(client, timestamp, datapath, CASE2_1)
    print "**** STARTED THE CLIENT'S FIRST ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client and then start it again.
    stopclient(client)
    saveclientkeys(client, server, timestamp, datapath, CASE2_1)
    stopPacketCapture(dut_client)

    print "Sleep 10s and then start the client again..."
    time.sleep(10)

    print "Start the packet capture again..."
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startPacketCapture(dut_client, capture_interface,
                       CASE2_2 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port 4433 or icmp\'"
                       )
    print "Sleep 2s and then start the client..."
    time.sleep(2)
    startclient(client, timestamp, datapath, CASE2_2)
    print "**** STARTED THE CLIENT'S SECOND ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client, server and packet capture .
    stopclient(client)
    saveclientkeys(client, server, timestamp, datapath, CASE2_2)
    stopserver(server)

    time.sleep(2)
    stopPacketCapture(dut_client)
    print "**** STOPPED THE CLIENT, SAVED THE KEYS, STOPPED THE SERVER AND PACKET CAPTURE ****\n\n"
    print "**** CASE 2 END ****"


def case3(client, server):
    print "case3"
    '''
    Case 3
    1. The Client connects with scid=1 and dcid=2
    and attempts to send "hello" and then closes
    the connection.
    2. The client connects with scid=1 and dcid=3
    and attempts to send "hello" and then closes
    the connection.
    '''
    #### Run the appropriate server from the below list
    '''
    The server list could change over time
    '''
    print "**** Clean up everything before starting ****"
    cleanup(servers, clients)
    print "\n\n"
    print "**** CASE 3 START ****"
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    datapath = prepareDataPath(dut_client, logtimestamp, CASE3, server)

    # First start the server
    startserver(server, timestamp, datapath, CASE3)
    print "**** STARTED THE SERVER *****\n\n"
    # exit()

    # Then start packet capture
    startPacketCapture(dut_client, capture_interface,
                       CASE3_1 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port 4433 or icmp\'"
                       )
    print "**** STARTED PACKET CAPTURE *****\n\n"

    print "Sleep 2s before starting..."
    time.sleep(2)

    # Start the client
    print "Now to start the client"
    # timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startclient(client, timestamp, datapath, CASE3_1)
    print "**** STARTED THE CLIENT'S FIRST ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client and then start it again.
    stopclient(client)
    saveclientkeys(client, server, timestamp, datapath, CASE3_1)
    stopPacketCapture(dut_client)

    print "Sleep 10s and then start the client again..."
    time.sleep(10)

    print "Start the packet capture again..."
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startPacketCapture(dut_client, capture_interface,
                       CASE3_2 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port 4433 or icmp\'"
                       )
    print "Sleep 2s and then start the client..."
    time.sleep(2)
    startclient(client, timestamp, datapath, CASE3_2)
    print "**** STARTED THE CLIENT'S SECOND ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client, server and packet capture .
    stopclient(client)
    saveclientkeys(client, server, timestamp, datapath, CASE3_2)
    stopserver(server)

    time.sleep(2)
    stopPacketCapture(dut_client)
    print "**** STOPPED THE CLIENT, SAVED THE KEYS, STOPPED THE SERVER AND PACKET CAPTURE ****\n\n"
    print "**** CASE 3 END ****"


def case4(client, server):
    print "case4"
    '''
    Case 4
    1. The Client connects with scid=1 and dcid=2
    and attempts to send "hello" and then closes
    the connection.
    2. The client connects with scid=3 and dcid=2
    and attempts to send "hello" and then closes
    the connection.
    '''
    #### Run the appropriate server from the below list
    '''
    The server list could change over time
    '''
    print "**** Clean up everything before starting ****"
    cleanup(servers, clients)
    print "\n\n"
    print "**** CASE 4 START ****"
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    datapath = prepareDataPath(dut_client, logtimestamp, CASE4, server)

    # First start the server
    startserver(server, timestamp, datapath, CASE4)
    print "**** STARTED THE SERVER *****\n\n"
    # exit()

    # Then start packet capture
    startPacketCapture(dut_client, capture_interface,
                       CASE4_1 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port 4433 or icmp\'"
                       )
    print "**** STARTED PACKET CAPTURE *****\n\n"

    print "Sleep 2s before starting..."
    time.sleep(2)

    # Start the client
    print "Now to start the client"
    # timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startclient(client, timestamp, datapath, CASE4_1)
    print "**** STARTED THE CLIENT'S FIRST ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client and then start it again.
    stopclient(client)
    saveclientkeys(client, server, timestamp, datapath, CASE4_1)
    stopPacketCapture(dut_client)

    print "Sleep 10s and then start the client again..."
    time.sleep(10)

    print "Start the packet capture again..."
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startPacketCapture(dut_client, capture_interface,
                       CASE4_2 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port 4433 or icmp\'"
                       )
    print "Sleep 2s and then start the client..."
    time.sleep(2)
    startclient(client, timestamp, datapath, CASE4_2)
    print "**** STARTED THE CLIENT'S SECOND ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client, server and packet capture .
    stopclient(client)
    saveclientkeys(client, server, timestamp, datapath, CASE4_2)
    stopserver(server)

    time.sleep(2)
    stopPacketCapture(dut_client)
    print "**** STOPPED THE CLIENT, SAVED THE KEYS, STOPPED THE SERVER AND PACKET CAPTURE ****\n\n"
    print "**** CASE 4 END ****"


def case5(client, server):
    print "case5"
    '''
    Case 5
    1. The Client connects with scid=1 and dcid=2
    and then issues $new_connection_ids$ with cids
    that are *all the same* as the scid (scid=1)
    and then attempts to send "hello" and then
    closes the connection.
    '''
    print "**** Clean up everything before starting ****"
    cleanup(servers, clients)
    print "\n\n"
    print "**** CASE 5 START ****"
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    datapath = prepareDataPath(dut_client, logtimestamp, CASE5, server)

    # First start the server
    startserver(server, timestamp, datapath, CASE5)
    print "**** STARTED THE SERVER *****\n\n"

    # Then start packet capture
    startPacketCapture(dut_client, capture_interface,
                       CASE5 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port 4433 or icmp\'"
                       )
    print "**** STARTED PACKET CAPTURE *****\n\n"

    print "Sleep 2s before starting..."
    time.sleep(2)

    # Start the client
    print "Now to start the client"
    # timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startclient(client, timestamp, datapath, CASE5)
    print "**** STARTED THE CLIENT'S FIRST ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client, server and packet capture .
    stopclient(client)
    saveclientkeys(client, server, timestamp, datapath, CASE5)
    stopserver(server)

    time.sleep(2)
    stopPacketCapture(dut_client)
    print "**** STOPPED THE CLIENT, SAVED THE KEYS, STOPPED THE SERVER AND PACKET CAPTURE ****\n\n"
    print "**** CASE 5 END ****"


def case6(client, server):
    print "case6"
    '''
    Case 6
    1. The Client connects with scid=1 and dcid=2
    and then issues $new_connection_ids$ with cids
    that are *all the same* but different from the
    initial scid and then attempts to send "hello"
    and then closes the connection.
    '''
    print "**** Clean up everything before starting ****"
    cleanup(servers, clients)
    print "\n\n"
    print "**** CASE 6 START ****"
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    datapath = prepareDataPath(dut_client, logtimestamp, CASE6, server)

    # First start the server
    startserver(server, timestamp, datapath, CASE6)
    print "**** STARTED THE SERVER *****\n\n"

    # Then start packet capture
    startPacketCapture(dut_client, capture_interface,
                       CASE6 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port 4433 or icmp\'"
                       )
    print "**** STARTED PACKET CAPTURE *****\n\n"

    print "Sleep 2s before starting..."
    time.sleep(2)

    # Start the client
    print "Now to start the client"
    # timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startclient(client, timestamp, datapath, CASE6)
    print "**** STARTED THE CLIENT'S FIRST ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client, server and packet capture .
    stopclient(client)
    saveclientkeys(client, server, timestamp, datapath, CASE6)
    stopserver(server)

    time.sleep(2)
    stopPacketCapture(dut_client)
    print "**** STOPPED THE CLIENT, SAVED THE KEYS, STOPPED THE SERVER AND PACKET CAPTURE ****\n\n"
    print "**** CASE 6 END ****"


def case7(client, server):
    print "case6"
    '''
    Case 7
    1. The Client connects with scid=1 and dcid=2
    and then issues $new_connection_ids$ with cids
    that are *all the same* as the
    initial dcid and then attempts to send "hello"
    and then closes the connection.
    '''
    print "**** Clean up everything before starting ****"
    cleanup(servers, clients)
    print "\n\n"
    print "**** CASE 7 START ****"
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    datapath = prepareDataPath(dut_client, logtimestamp, CASE7, server)

    # First start the server
    startserver(server, timestamp, datapath, CASE7)
    print "**** STARTED THE SERVER *****\n\n"

    # Then start packet capture
    startPacketCapture(dut_client, capture_interface,
                       CASE7 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port 4433 or icmp\'"
                       )
    print "**** STARTED PACKET CAPTURE *****\n\n"

    print "Sleep 2s before starting..."
    time.sleep(2)

    # Start the client
    print "Now to start the client"
    # timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startclient(client, timestamp, datapath, CASE7)
    print "**** STARTED THE CLIENT'S FIRST ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client, server and packet capture .
    stopclient(client)
    saveclientkeys(client, server, timestamp, datapath, CASE7)
    stopserver(server)

    time.sleep(2)
    stopPacketCapture(dut_client)
    print "**** STOPPED THE CLIENT, SAVED THE KEYS, STOPPED THE SERVER AND PACKET CAPTURE ****\n\n"
    print "**** CASE 7 END ****"


def main():
    print "main()"
    print "Script started at: " + str(logtimestamp)

    # cleanup(servers, clients)

    logs(logtimestamp)

    for server in servers:
        if server == "aioquic" or server == "nginx" or server == "msquic" or server == "mvfst"\
                or server == "pquic" or server == "picoquic" or server == "cloudflare":
            client = "lsquic_http"
        else:
            client = "lsquic"
        case1(client, server)
        # case2(client, server)
        # case3(client, server)
        # case4(client, server)
        # case5(client, server)
        # case6(client, server)
        # case7(client, server)


def debug():
    print "debug()"
    #### Packet capture
    # startPacketCapture(dut, capture_interface, "foo.pcap",
    #                    scriptPath+"ExpLib_logs/"+logtimestamp+"/",
    #                    "micro",
    #                    "\'udp\'"
    #                    )
    # # time.sleep(1)
    # stopPacketCapture(dut)
    # cleanup(servers, clients)
    # startclient("lsquic_http", timestamp="now", datapath="/tmp/", case=CASE1)
    # stopserver("quant")
    # startserver("quant", timestamp="now", datapath="/tmp/", case=CASE5)
    # startclient("lsquic", timestamp="now", datapath="/tmp/", case=CASE5)
    # stopserver("quant")
    # ssh = SSHConnect(dut_server)
    # ssh.close()
    # for s in ["quant"]:
    # for s in servers:
    server = servers[8]
    # stopserver(server)
    # exit()
    # client = "lsquic_http"
    client = "lsquic"
    timestamp = "now" + server + client
    datapath = "/root/QUIC/Experiments/data/tmp/"
    startserver(server, timestamp=timestamp, datapath=datapath, case=CASE5)
    startPacketCapture(dut_client, capture_interface,
                       CASE5 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port 4433 or icmp\'"
                       )
    startclient(client, timestamp=timestamp, datapath=datapath, case=CASE5)
    time.sleep(5)
    stopPacketCapture(dut_client)
    stopserver(server)

if __name__ == "__main__":
    main()
    # debug()

