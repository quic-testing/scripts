#!/usr/bin/env bash
#timestamp=$1
bash case1-analysis.sh $1
bash filter-logs.sh $1
#bash passive-case1-analysis.sh $1
#bash passive-analysis.sh $1

# EXTRACT TEST SPECIFIC FINGERPRINT
#for method in active passive
for method in active
do
#for case in case1 case5 case6
for case in case1 case5 case6
do
if [[ $method == "active" ]]; then
bash extract-test-results.sh $1 $case $method
elif [[ $method == "passive" && $case == "case5" ]]; then
bash extract-test-results.sh $1 $case $method
elif [[ $method == "passive" && $case == "case1" ]]; then
bash extract-test-results.sh $1 $case $method
else
continue
fi
done
done

# ACTIVE
# table from case5
# ipl cidlen cidchange errorcode
# table from case6
# errorcode
# table from case1
# noserverpackets
# PASSIVE
# tables from case5
# ipl cidlen cidchange
# table from case1
# noserverpackets

echo "generate tables"
#for method in active passive;
for method in active;
do
echo "Method:"$method
for CASE in case5 case6 case1;
do
echo "Case:"$CASE
for test in ipl cidlen cidchange errorcode noserverpackets connecttoserver
do

if [[ $method == "active" && $CASE == "case1" && $test != "errorcode" ]]; then
echo "Test:"$test
bash generate-latex-table.sh $1 $CASE $test $method
elif [[ $method == "active" && $CASE != "case1" && $test == "errorcode" ]]; then
echo "Test:"$test
bash generate-latex-table.sh $1 $CASE $test $method
elif [[ $method == "passive" && $CASE == "case1" && $test != "errorcode" ]]; then
echo "Test:"$test
bash generate-latex-table.sh $1 $CASE $test $method
else
echo "NO MATCHING METHOD, CASE & TEST"
fi
done
done
done

# GRAPH VISUALIZATION
#for case in case1 case5 case6
#do
#bash fingerprint.sh $1 $case
#bash generate-signature.sh $1 $case-identical
#done
