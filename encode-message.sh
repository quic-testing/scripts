#!/usr/bin/env bash
MESSAGE=$1
ENCODED_MESSAGE=$1-encoded
DECODED_MESSAGE=$1-decoded
echo "MESSAGE"
cat $MESSAGE
echo "CONVERT ASCII MESSAGE TO BINARY AND SAVE IN FILE"
cat $MESSAGE | perl -lpe '$_=join "\n", unpack"(B8)*"' | sed 's/^./1/g' > $ENCODED_MESSAGE
cat $ENCODED_MESSAGE

echo "CONVERT BINARY TO ASCII AND SAVE IN FILE"
cat $ENCODED_MESSAGE | sed 's/^./0/g' | perl -lpe '$_=join "\n", pack"B*",$_' > $DECODED_MESSAGE
cat $DECODED_MESSAGE
