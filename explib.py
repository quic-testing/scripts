import paramiko as pk
import time
import sys
import os
import errno
from datetime import datetime

#--------Log Colors---------------------
RED = "\033[1;31m"
BLUE = "\033[1;34m"
RESET = "\033[0;0m"

#--------Logs Vars----------------------
scsName = "test-cid"
CmdLogPath = ""
DefaultCmdLogPath = "/tmp/ExpLib_logs/temp_log.txt"
SummaryPath = ""
# packet_capture_path = "/tmp/"
imacpath = "/Users/hashkash-imac/Documents/HUB/my_work/QUIC/"
quicvmpath = "/root/QUIC/"
packetDataPath = quicvmpath + "Experiments/Data/"

#--------Paths ----------------------
tcpdump_path="/usr/sbin/"

#---------- Server IPs------------------
localhost = "127.0.0.1"
quicvm = "127.0.0.1"

#------------Forwording Ports-----------
default_port = "22"

#------------Connexions-----------------
#--------[NAME,IP,PORT,USER]------------
#++++++++++++++Servers++++++++++++++++++
# TODO: Create standard account with lower privileges to ssh and run the scripts
cnx_localhost = ["localhost", localhost, default_port, "hashkash-imac"]
# cnx_quicvm = ["localhost", localhost, 2200, "root"]
cnx_quicvm = ["localhost", localhost, 22, "root"]
k = pk.RSAKey.from_private_key_file(quicvmpath + "Experiments/scripts/keys/id_rsa")


#--------Global Parameters--------------
ssh_timeout = 600

########################################
#--------SSH Connection-----------------
########################################

def SSHConnect(cnx):
    timeout_start = time.time()
    while time.time() < timeout_start + ssh_timeout:

        try:
            client = pk.client.SSHClient()
            client.set_missing_host_key_policy(pk.AutoAddPolicy())
            #client.connect(cnx[1], int(cnx[2]), username=cnx[3])
            client.connect(hostname="localhost", username=cnx[3], port=cnx[2], pkey=k)
            return client

        except pk.ssh_exception.NoValidConnectionsError:
            sys.exc_clear()
            time.sleep(20)
        except pk.ssh_exception.BadAuthenticationType:
            printLog ("EXCEPTION: Authentication Problem ... ! ", RED)
            break

    printLog("Timeout: unable to connect to: "+cnx[0]+"  ....", RED)


def RunCommand(sshClient, cmd):
    try:
        stdin, stdout, stderr = sshClient.exec_command(cmd)
        output = stdout.readlines()
        error = stderr.readlines()

        printCmdLog("----------------------------- \n")
        printCmdLog("Command: \n")
        printCmdLog(cmd+"\n")
        printCmdLog("Output: \n")
        printCmdLog("\n".join(output))
        printCmdLog("----------------------------- \n")

        # print "Error: "
        # import pprint
        # pprint.pprint(error)

        '''
        RunCommand currently does not return
        True or False if the command executed correctly.
        So the caller needs to verify if
        it was True or not.
        '''
        if error:
            printLog("Info/error from: \n", RED)
            printCmdLog("Info/error from: \n")

            printLog("                  "+cmd+" \n", BLUE)
            printCmdLog("                  "+cmd+" \n")

            printLog("Info/error message: ", RED)
            printCmdLog("Info/error message: ")

            printLog("\n".join(error), BLUE)
            printCmdLog("\n".join(error))

        return output, error

    except AttributeError:
        printLog("EXCEPTION: The SSH Session might be DEAD ! ", RED)


def printLog(message, color):
    sys.stdout.write(color)
    print(message)
    sys.stdout.write(RESET)


def printCmdLog(strg):
    if len(CmdLogPath) != 0:
        if not os.path.exists(os.path.dirname(CmdLogPath)):
            try:
                os.makedirs(os.path.dirname(CmdLogPath))
            except OSError as exc:
                if exc.errno != errno.EEXIST:
                    raise
        with open(CmdLogPath, "a") as f:
            f.write(strg)
            f.close()
    else:
        if not os.path.exists(os.path.dirname(DefaultCmdLogPath)):
            try:
                os.makedirs(os.path.dirname(DefaultCmdLogPath))
            except OSError as exc:
                if exc.errno != errno.EEXIST:
                    raise
        with open(DefaultCmdLogPath, "a") as f:
            logTimeStamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
            f.write("##################  "+logTimeStamp+"  ################## \n")
            f.write(strg)
            f.close()


'''
Creates Log Files for the running Scenario
'''


def logs(timestamp):
    global CmdLogPath
    global SummaryPath
    global packet_capture_path

    CmdLogPath = "./ExpLib_logs/" + timestamp + "/" + scsName + ".txt"
    SummaryPath = "./ExpLib_logs/" + timestamp + "/" + scsName + "-summary.txt"
    packet_capture_path = "./ExpLib_logs/" + timestamp + "/"
    print packet_capture_path

    if not os.path.exists(os.path.dirname(SummaryPath)):
        try:
            os.makedirs(os.path.dirname(SummaryPath))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    # with open(SummaryPath, "w") as f:
    #     f.write(Summary)
    #     f.close()


'''Capture packets on a specific interface,
write them to a specified file with timestamp
precision and filters using a specific packet
capture tool (default is tcpump)'''

def startPacketCapture(cnx, interface, fileName,
                       pcapDataPath,
                       timestamp_precision="micro",
                       filters="",
                       tool="tcpdump"):
    print "Going to start the packet capture on " + cnx[0] + " on interface:" + interface
    ssh = SSHConnect(cnx=cnx)
    if tool == "tcpdump":
        print "start capture with tcpdump"
        RunCommand(ssh, "screen -dmSL tcpdumpSession " + tcpdump_path +\
                   "tcpdump -neK -i " + interface + " -w " + \
                   pcapDataPath + fileName +
                   " --time-stamp-precision="+timestamp_precision +
                   " " + filters)
    elif tool == "dagbits":
        print "start capture with dagbits"
        RunCommand(ssh, "screen -dmSL dagbitsSession bash -c " + \
                   "'dagbits -d " + interface + " > " + \
                   pcapDataPath + fileName + "'" )
    elif tool == "dagsnap":
        print "start capture with dagsnap"
        RunCommand(ssh, "screen -dmSL dagsnapSession bash -c " + \
                   "'dagsnap -d "+interface+" -o "+ \
                   pcapDataPath + fileName + "'")
        ssh.close()


def stopPacketCapture(cnx):
    print "Going to stop the packet capture on " + cnx[0]
    ssh = SSHConnect(cnx=cnx)
    RunCommand(ssh, "screen -S tcpdumpSession -X stuff $'\003'")
    RunCommand(ssh, "pkill tcpdump")
    # RunCommand(ssh, "screen -ls | grep Detached | cut -d . -f1 | awk '{print $1}' | xargs kill")
    ssh.close()


def prepareDataPath(cnx, date, case, server):
    print "preparePcapDataPath()"
    ssh = SSHConnect(cnx)
    path = packetDataPath + date + "/" + case + "/" + server + "/"
    RunCommand(ssh, "mkdir -p " + path)
    ssh.close()
    return path


