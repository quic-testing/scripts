# Process the connection ids from the visited alexa top 1M websites
import json, sys, pprint


def get_client_generated_data(event):
    # print "get_client_generated_connid"
    # print "client event id" + str(event["params"]["source"]["id"])
    # print "client event id" + str(event["source"]["id"])
    host = event["params"]["host"]
    connectionid = event["params"]["connection_id"]
    netlogid = event["source"]["id"]
    return {netlogid: [host, connectionid]}


def get_server_generated_data(event):
    # print "get_server_generated_connid"
    # pprint.pprint(event["source"]["id"])
    # packet from the server need to be sorted as
    # there are multiple quic sessions. We want to
    # match connections to their respective hosts.
    # Hence, use the event[source][id] to track
    # each quic_session.
    connectionid = event["params"]["connection_id"]
    netlogid = event["source"]["id"]
    return {netlogid: connectionid}


print "start"
websitespath = "/Users/hashkash-imac/Documents/HUB/my_work/QUIC/Experiments/analysis/Chromium/"
websitefile = "QUIC_SESSION_PACKET_RECEIVED_TYPE_238"
datapath = "/Users/hashkash-imac/Documents/HUB/my_work/QUIC/Experiments/data/Chromium/"
analysispath = "/Users/hashkash-imac/Documents/HUB/my_work/QUIC/Experiments/analysis/Chromium/"
analysisfile = "client-server-connid-map.json"

keys = []

websites = open(websitespath + websitefile, "r")
for website in websites:
    website = website.strip('\n')
    keys.append(website)
websites.close()

websitesdict = dict.fromkeys(keys)
websites = open(websitespath + websitefile, "r")
client_connectionid_map = dict.fromkeys(keys)
server_connectionid_map = dict.fromkeys(keys)

websites = open(websitespath + websitefile, "r")
for website in websites:
    print website
    website = website.strip('\n')
    data = json.loads(open(datapath + website).read())
    events = data["events"]
    #pprint.pprint(data["events"])
    quic_session_eventlist = []
    quic_session_unauth_pkt_rcvd_eventlist = []
    connectionid_list = []
    for event in events:
        #pprint.pprint(event['type'])
        if event['type'] == 234 and event['phase'] == 1:
            #print "type:234"
            #pprint.pprint(event)
            if client_connectionid_map[website] is None:
                client_connectionid_map[website] = get_client_generated_data(event)
            else:
                client_connectionid_map[website].update(get_client_generated_data(event))
        if event['type'] == 243:
            #print "type:243"
            # quic_session_unauth_pkt_rcvd_eventlist.append(event)
            if server_connectionid_map[website] is None:
                server_connectionid_map[website] = get_server_generated_data(event)
            else:
                server_connectionid_map[website].update(get_server_generated_data(event))
            #pprint.pprint(event)
    # print "Client side"
    # pprint.pprint(client_connectionid_map)
    # print "Server side"
    # pprint.pprint(server_connectionid_map)
    websitesdict = {'client': client_connectionid_map, 'server': server_connectionid_map}
    # break

# for k in websitesdict.keys():
#     if websitesdict[k] is not None:
#         pprint.pprint(websitesdict[k])

pprint.pprint(websitesdict)
f = open(analysispath + analysisfile, 'w')
f.write(json.dumps(websitesdict))
print "done."


