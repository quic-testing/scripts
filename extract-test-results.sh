#!/usr/bin/env bash
WD=./
DATADIR=../data/
ANALYSISDIR=../analysis/
DATATIMESTAMP=$1
CASE=$2
METHOD=$3
cd $ANALYSISDIR/$DATATIMESTAMP/$METHOD

for i in $CASE-*fingerprint*out;
do
filename=$(echo $i | cut -d "-" -f 1-2)
if [[ $CASE == "case1" ]]; then
grep "FIRST_SERVER_IP_LEN" $i > $filename-ipl.fingerprint
grep "CID_LENGTH" $i > $filename-cidlen.fingerprint
grep "IS_CID_CHANGED" $i > $filename-cidchange.fingerprint
grep "CASE1_2_NO_PACKETS_FROM_SERVER" $i > $filename-noserverpackets.fingerprint
grep "CASE1_2_CONNECT_TO_SERVER" $i > $filename-connecttoserver.fingerprint
else
grep "ERROR_CODE" $i > $filename-errorcode.fingerprint
fi
done

