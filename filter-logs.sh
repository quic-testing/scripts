#!/bin/bash
WD=./
DATADIR=../data/
ANALYSISDIR=../analysis/
DATATIMESTAMP=$1
PORT=4433

cd $DATADIR/$DATATIMESTAMP

for case in case5 case6
do
echo $case
#for server in "quicly" "aioquic" "quant" "cloudflare" "ngtcp2" "nginx" "mvfst" "pquic" "neqo" "picoquic" "lsquic"
for server in "f5" "quinn" "akamai" "ats" "chromium"
do

echo $server;
pwd;
ls $case/$server;

FINGERPRINT_FILE=$case-$server-fingerprint-$DATATIMESTAMP.out
echo $FINGERPRINT_FILE
#echo "$case-$server, " > $FINGERPRINT_FILE
scid_file=$case/$server/$case*scid*.out
case_pcap_file=$case/$server/$case*.pcap

is_qlog=0
result=$(grep "qlog" $scid_file -c)
if [[ $result -gt "0" ]]; then is_qlog=$result; fi

is_http=0
result=$(grep -e "HTTP" $scid_file -c)
if [[ $result -gt "0" ]]; then is_http=$result; fi
# ASSUMING THE CORRECT CLIENT WAS USED FOR TESTING
# NEED THIS AS SOMETIMES THERE ARE PACKETS BUT NO HTTP RESPONSE
result=$(echo $scid_file | grep -c "http")
if [[ $result == "1" ]]; then
is_http=1
fi

timestamp=$(ls $case/$server/$case*keys | cut -d "-" -f 4-8 | cut -d "." -f 1)
echo "TIMESTAMP="$timestamp
if [[ $is_http == "0" ]]; then
keys_file=$case/$server/$case-lsquic-$server-$timestamp.keys
case_pcap_file=$case/$server/$case-lsquic-$server-$timestamp.pcap
else
keys_file=$case/$server/$case-lsquic_http-$server-$timestamp.keys
case_pcap_file=$case/$server/$case-lsquic_http-$server-$timestamp.pcap
fi

PORT=4433
if [[ $server == "akamai" || $server == "quinn" ]]; then PORT=443; fi

#echo "SCID_FILE is:"
#cat $scid_file
#continue

is_qlog=0
result=$(grep -e "qlog" $scid_file -c)
if [[ $result -gt "0" ]]; then
is_qlog=$result;
fi
echo "IS_QLOG=$result, " >> $FINGERPRINT_FILE

is_http=0
result=$(grep -e "HTTP" $scid_file -c)
if [[ $result -gt "0" ]]; then is_http=$result; fi
echo "IS_HTTP=$result, " >> $FINGERPRINT_FILE

# TODO: Need to include difference for echo and http as for http, don't find the ncid
#is_ncid_connclose=0
#echo "TEST FOR NEW_CONNECTION_ID JUST BEFORE RECEIVING A FRAME_TYPE CONNCLOSE"
#result=$(grep -e "qlog" $scid_file | grep -e "\"frame_type\":\"CONNECTION_CLOSE\"" -B 1 | grep -e "\"frame_type\":\"NEW_CONNECTION_ID\"" -c)
#if [[ $result -gt "0" ]]; then is_ncid_connclose=$result; fi
#echo "IS_NCID_CONNCLOSE=$result," >> $FINGERPRINT_FILE
# This is where we can add our first server to the set of implementations
#implementations="quicly, quant, "


is_qlog_frame_type=0
echo "TEST FOR HANDSHAKE_DONE AT THE CLIENT"
result=$(grep -e "\"frame_type\":\"HANDSHAKE_DONE\"" $scid_file -c);
if [[ $result -gt "0" ]]; then is_qlog_frame_type="HANDSHAKE_DONE"; fi
echo "IS_HANDSHAKE_DONE=$result," >> $FINGERPRINT_FILE

# FIRST CHECK THE CLIENT'S LOG FOR THE CONNECTION_CLOSE MESSAGE
# THEN CHECK THE PCAP
# EXTRACT THE ERROR CODE FROM BOTH
echo "TEST FOR CONNECTION_CLOSE AT THE CLIENT"
is_conn_close_client=0
result=$(grep -e "conn: Received CONNECTION_CLOSE frame (transport-level code:" $scid_file -c);
if [[ $result -gt "0" ]]; then
is_conn_close_client=$(grep -e "conn: Received CONNECTION_CLOSE frame (transport-level code:" $scid_file);
fi
error_code=$(echo ${is_conn_close_client} | cut -d "(" -f 2 | cut -d " " -f 3 | cut -d ";" -f 1)
if [[ $error_code != "" ]]; then
echo "FOUND ERROR CODE IN THE CLIENT LOG, NO NEED TO CHECK THE PCAP"
else
echo "TEST FOR CONNECTION_CLOSE IN THE PCAP"
result=$(tshark -r $case_pcap_file -o tls.keylog_file:$keys_file -V | grep "CONNECTION_CLOSE (Transport) Error code:" -c)
if [[ $result -gt "0" ]]; then
is_conn_close_pcap=$(tshark -r $case_pcap_file -o tls.keylog_file:$keys_file -V | grep "CONNECTION_CLOSE (Transport) Error code:");
fi
#echo $is_conn_close_pcap
error_code=$(echo ${is_conn_close_pcap} | cut -d " " -f 5)
fi
echo "ERROR_CODE=$error_code, " >> $FINGERPRINT_FILE;


done
done

cat /dev/null > fingerprints.out
for i in case*fingerprint-$DATATIMESTAMP.out;
do
echo $i >> fingerprints.out
cat $i >> fingerprints.out
done

echo "Make the respective analysis directory"
mkdir -p ../../analysis/$DATATIMESTAMP/active
echo "Move the fingerprint files to the analysis directory"
mv *fingerprint* ../../analysis/$DATATIMESTAMP/active/

