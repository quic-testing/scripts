#!/bin/bash

#### LOG FILTERS ####
# Can use the -c option from grep to count the number of matches
result=$(grep -nre "qlog" case5 | grep -ne "\"frame_type\":\"CONNECTION_CLOSE\"" -B 1 | grep -e "\"frame_type\":\"NEW_CONNECTION_ID\"" -c)

# Connection close
grep -nre "\"frame_type\":\"CONNECTION_CLOSE\""
# Handshake done
grep -nre "\"frame_type\":\"HANDSHAKE_DONE\""
# New connection id
grep -nre "\"frame_type\":\"NEW_CONNECTION_ID\""

# For quant
grep -nre "{\"frame_type\":\"RETIRE_CONNECTION_ID\"},{\"frame_type\":\"HANDSHAKE_DONE\"}"

# For aioquic/http based quic servers
grep -nr -m 1 -e "HTTP"

# For lsquic in case 5
grep -nre "NEW_CONNECTION_ID: received the same CID with sequence numbers 0 and 1"

# For lsquic in case 6
grep -nre "NEW_CONNECTION_ID: received the same CID with sequence numbers 1 and 2"

# For lsquic case 1, second attempt should find this line exactly once
grep -nre "sendctl: consider handshake packets lost: 1 resubmitted"
# and exactly one of this
grep -nre "\"CONNECTIVITY\",\"VERNEG\",\"LINE\",{\"proposed_version\":\"FF00001D\"}"

# For quicly case 5 will have a NEW_CONNECTION_ID followed by CONNECTION_CLOSE
result=$(grep -nre "qlog" case5 | grep -ne "\"frame_type\":\"CONNECTION_CLOSE\"" -B 1 | grep -e "\"frame_type\":\"NEW_CONNECTION_ID\"" -c)
if [ $result == "1" ]; then echo "quicly"; fi

result = $(grep -nre "\"frame_type\":\"HANDSHAKE_DONE\"" .)
echo $result

# Below cut command will print characters from the output starting from position 14 till the end
cut -c 14-

# Check out the below to see which ones actually have the HANDSHAKE_DONE qlog
#hashkash-imac:~/Documents/HUB/my_work/QUIC/Experiments/data/2020-09-04_14-24-04$
for i in *;
do
for j in $i/*;
do
for k in $j/*scid*;
do
result=$(grep -nre "\"frame_type\":\"HANDSHAKE_DONE\"" $k -c);
echo $result
done
done
done
#for i in *; do for j in $i/*; do  for k in $j/*scid*; do result=$(grep -nre "\"frame_type\":\"HANDSHAKE_DONE\"" $k -c);echo $result; done; done; done
#case1/lsquic/case1-lsquic-scid1_dcid2-2020-09-04_14-24-09.out:1
#case1/lsquic/case1-lsquic-scid1_dcid2-2020-09-04_14-24-22.out:0
#case1/quant/case1-lsquic-scid1_dcid2-2020-09-04_14-25-30.out:1
#case1/quant/case1-lsquic-scid1_dcid2-2020-09-04_14-25-43.out:1
#case1/quicly/case1-lsquic-scid1_dcid2-2020-09-04_14-24-50.out:1
#case1/quicly/case1-lsquic-scid1_dcid2-2020-09-04_14-25-02.out:1
#case5/lsquic/case5-lsquic-scid1_dcid2_newscid1-2020-09-04_14-24-31.out:1
#case5/quant/case5-lsquic-scid1_dcid2_newscid1-2020-09-04_14-25-52.out:0
#case5/quicly/case5-lsquic-scid1_dcid2_newscid1-2020-09-04_14-25-12.out:0
#case6/lsquic/case6-lsquic-scid1_dcid2_newscid3-2020-09-04_14-24-40.out:1
#case6/quant/case6-lsquic-scid1_dcid2_newscid3-2020-09-04_14-26-01.out:0
#case6/quicly/case6-lsquic-scid1_dcid2_newscid3-2020-09-04_14-25-21.out:0


#### TCPDUMP FILTERS ####

initialserverplen=$(tcpdump -neK -# -M *-0100000000000000.keys -r *.pcap src port 4433 | head -n1 | awk {'print $10'} | cut -d ":" -f 1)

if [ $initialserverplen == "153" ]; then
echo "lsquic"
elif [ $initialserverplen == "140" ]; then
echo "quicly"
elif [ $initialserverplen == "1514" ]; then
echo "quant"
elif [ $initialserverplen == "1322" ]; then
echo "aioquic"
elif [ $initialserverplen == "197" ]; then
echo "cloudflare"
elif [ $initialserverplen == "1294" ]; then
echo "mvfst | picoquic | pquic"
elif [ $initialserverplen == "785" ]; then
echo "neqo"
elif [ $initialserverplen == "193" ]; then
echo "nginx"
elif [ $initialserverplen == "1067" ]; then
echo "ngtcp2"
echo "$initialserverplen is unknown"
fi

tshark -r case1-lsquic-quant-2020-09-04_14-25-27.pcap

# To decrypt the packets use the -o ssl.keylog_file option
tshark -nr case5-lsquic-lsquic-2020-09-04_14-24-29.pcap -o
ssl.keylog_file:case5-lsquic-2020-09-04_14-24-31-0100000000000000.keys

# Use the -V option to print all the packet fields

for i in case1_1 case1_2; do echo "**** $i ****";tshark -r $i*.pcap -o tls.keylog_file:$i*.keys; done

#QUIC/Experiments/data/2020-09-11_13-41-40/case1$
for j in *; do for i in $j/case1_1 $j/case1_2; do echo "**** $i ****"; tshark -r $i*.pcap -o tls.keylog_file:$i*.keys ; done; done


####### All filter commands one after the other


