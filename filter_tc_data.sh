#!/usr/bin/env bash
source test_cases.sh
timestamp=$1
LOG=$TCPATH/tc-analysis-${timestamp}

for i in $LOG*found-line
do
result=$(echo $i | cut -d "-" -f9)
if [[ $result == "ack" || $result == "connection" ]]; then
continue
else

echo "**** $result ****" >> filtered

cut -d "/" -f5- $i | cut -d ":" -f 1,3 >> filtered

#echo $line | cut -d "/" -f4 >> foo
#cut -d ":" -f3 foo >> foo1


fi
done