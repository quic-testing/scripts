#!/usr/bin/bash
WD=./
DATADIR=../data/
ANALYSISDIR=../analysis/
DATATIMESTAMP=$1
cd $ANALYSISDIR/$DATATIMESTAMP
CASE=$2
TEST=$3

# TODO: USE THE COMBINATION OF CASE AND TEST TO GENERATE THE RESPECTIVE GRAPH
cat /dev/null > $CASE-identical.dot

echo "graph g {" >> $CASE-identical.dot

for S_i_j in $CASE*fingerprint-servers.out
do
#echo "S_i_j="$S_i_j
for S_j_i in $CASE*fingerprint-servers.out
do
#echo "S_j_i="$S_j_i


if [[ $S_i_j == $S_j_i ]]; then
echo "Skip self comparison"
echo $S_i_j" and "$S_j_i
s_ij=$(echo ${S_i_j} | cut -d "-" -f 2)
s_ji=$(echo ${S_j_i} | cut -d "-" -f 2)
echo $s_ij" -- "$s_ji >> $CASE-identical.dot
break
fi

result=$(diff --minimal --brief --report-identical-files $S_i_j $S_j_i | grep -e "identical" -c)
echo $result
if [[ $result -gt "0" ]]; then
s_ij=$(echo ${S_i_j} | cut -d "-" -f 2)
s_ji=$(echo ${S_j_i} | cut -d "-" -f 2)
echo $s_ij" -- "$s_ji >> $CASE-identical.dot
fi

done
# Break here to end after running through j_i but only one of i_j
#break
done
echo "}" >> $CASE-identical.dot
