#!/usr/bin/env bash
LSQUIC


CASE 1

Over here, need to check for each file separately and save the result
#for i in *; do for j in $i/*; do  for k in $j/*scid*; do result=$(grep -nre "\"frame_type\":\"HANDSHAKE_DONE\"" $k -c);echo $result; done; done; done
result1=$(grep -nre "\"frame_type\":\"HANDSHAKE_DONE\"" $k -c)
result2=$(grep -nre "\"frame_type\":\"HANDSHAKE_DONE\"" $k -c)
if [ $result1 == "1" && $result2 == "0" ] then
echo "LSQUIC"
fi

In the second attempt
result2=$(grep -nre "sendctl: consider handshake packets lost: 1 resubmitted" -c)
if [ $result2 == "1" ] then
echo "LSQUIC"
fi
# and exactly one of this
result2=$(grep -nre "\"CONNECTIVITY\",\"VERNEG\",\"LINE\",{\"proposed_version\":\"FF00001D\"}" -c)
if [ $result2 == "1" ] then
echo "LSQUIC"
fi


CASE 5

#for i in *; do for j in $i/*; do  for k in $j/*scid*; do result=$(grep -nre "\"frame_type\":\"HANDSHAKE_DONE\"" $k -c);echo $result; done; done; done
result=$(grep -nre "\"frame_type\":\"HANDSHAKE_DONE\"" $k -c)
if [ $result == "1" ] then
echo "LSQUIC"
fi

result=$(grep -nre "NEW_CONNECTION_ID: received the same CID with sequence numbers 0 and 1" -c)
if [ $result == "1" ] then
echo "LSQUIC"
fi

CASE 6

#for i in *; do for j in $i/*; do  for k in $j/*scid*; do result=$(grep -nre "\"frame_type\":\"HANDSHAKE_DONE\"" $k -c);echo $result; done; done; done
result=$(grep -nre "\"frame_type\":\"HANDSHAKE_DONE\"" $k -c)
if [ $result == "1" ] then
echo "LSQUIC"
fi

result=$(grep -nre "NEW_CONNECTION_ID: received the same CID with sequence numbers 1 and 2" -c)
if [ $result == "1" ] then
echo "LSQUIC"
fi



QUICLY

CASE 1
Should have the same result for both attempts, hence $result == "2"

result=$(grep -nre "qlog" case1 | grep -ne "\"frame_type\":\"HANDSHAKE_DONE\"" -B 1 | grep -e "\"frame_type\":\"NEW_CONNECTION_ID\"" -c)
if [ $result == "2" ]; then echo "quicly"; fi

CASE 5 AND CASE 6

result=$(grep -nre "qlog" case5 | grep -ne "\"frame_type\":\"CONNECTION_CLOSE\"" -B 1 | grep -e "\"frame_type\":\"NEW_CONNECTION_ID\"" -c)
if [ $result == "1" ]; then echo "quicly"; fi

AIOQUIC

CASE 1
result=$(grep -nr -m 1 -e "HTTP" -c)
if [ $result == "1" ]; then echo "aioquic"; fi

CASE 5

result=$(grep -nr -m 1 -e "HTTP" -c)
if [ $result == "1" ]; then echo "aioquic"; fi

CASE 6

result=$(grep -nr -m 1 -e "HTTP" -c)
if [ $result == "1" ]; then echo "aioquic"; fi


QUANT

CASE 1
will have the same result for both attempts. For quant, will also find the retire_connection_id twice

result=$(grep -nre "qlog" case1 | grep -ne "\"frame_type\":\"HANDSHAKE_DONE\"" -B 1 | grep -e "\"frame_type\":\"NEW_CONNECTION_ID\"" -c)
if [ $result == "2" ]; then echo "quicly"; fi
result=$(grep -nre "{\"frame_type\":\"RETIRE_CONNECTION_ID\"},{\"frame_type\":\"HANDSHAKE_DONE\"}" -c)
if [ $result == "2" ]; then echo "quant"; fi


CASE 5


CASE 6
