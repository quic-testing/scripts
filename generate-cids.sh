#!/bin/bash
QUIC_PATH="/root/QUIC/Code/"
LSQUIC_SRC_PATH=$QUIC_PATH"lsquic/src/"
LSQUIC_CONN_PATH=$LSQUIC_SRC_PATH"liblsquic/"
LSQUIC_BIN_PATH=$QUIC_PATH"lsquic/build/"

cd $LSQUIC_BIN_PATH

# BINARY REWRITE METHOD
#echo "Rewrite the echo and http client with CID:"$2 "and Length:"$3
#bbe -b "/\xc6\x40\x08\x02/:4" -e "r 3 \x$CID" -e "-r 2 \x$3" -o echo_client_scid1_dcid$2.out echo_client_scid1_dcid2
# TO CHECK FOR THE CID REWRITE USE OBJDUMP -D
#for i in *_localhost_scid* ; do echo $i; objdump -d $i | grep -e "c6 40 08 03"; done

# FOR NOW, JUST THE CID REWRITE SUFFICES. THE LENGTH CAN REMAIN 8 BYTES
for i in `seq 3 30`
do
CID=$(printf '%x' ${i})
echo $CID
bbe -b "/\xc6\x40\x08\x02/:4" -e "r 3 \x$CID" -o echo_client_scid1_dcid$i echo_client_scid1_dcid2
chmod +x echo_client_scid1_dcid$i
done

# SOURCE CODE MODIFICATION METHOD
#echo "Modify the source code"
#for i in `seq 2 12`; do
#cd $LSQUIC_CONN_PATH
#sed "264s/idbuf = [0-9]*/idbuf = $i/g" lsquic_conn.c > lsquic_conn$i.c
#cp lsquic_conn$i.c lsquic_conn.c
#cd ../../build
#make -j
#cp echo_client echo_client_scid1_dcid$i
#cp http_client http_client_scid1_dcid$i
#done
