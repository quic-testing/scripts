#!/usr/bin/env bash
DATATIMESTAMP=$1 #e.g., servers
CASE=$2 #e.g., case5
TEST=$3 #e.g., cid-len
METHOD=$4
LATEX_TABLE_FILE=$CASE-$TEST".tex"
LATEX_TABLE_PATH="../analysis/"$DATATIMESTAMP"/"$METHOD"/"

cd $LATEX_TABLE_PATH
echo "clear "
cat /dev/null > $LATEX_TABLE_FILE
echo "Now populate the table..."
echo "\begin{table}[t!]" >> $LATEX_TABLE_FILE
echo "\tiny" >> $LATEX_TABLE_FILE
echo "\begin{tabular}{@{}p{1.0cm}p{1.0cm}p{1.0cm}p{2.5cm}@{}}" >> $LATEX_TABLE_FILE
echo "\toprule" >> $LATEX_TABLE_FILE
echo "QUIC server & $TEST & Commit & Language \\\ \midrule" >> $LATEX_TABLE_FILE
for finger in $CASE*$TEST*fingerprint;
do
server=$(echo $finger | cut -d "-" -f 2)
test=$(cat $finger | cut -d "=" -f 2 | cut -d "," -f 1)
#if [[ $CASE == "case1" && $TEST == "noserverpackets" || $TEST == "connecttoserver" ]]; then
echo $server "&" $test "&" "&" "\\\\" >> $LATEX_TABLE_FILE
#elif [[ $TEST == "errorcode" ]]; then
#echo $server "&" $test "&" "&" "\\\\" >> $LATEX_TABLE_FILE
#elif [[ $CASE == "case5" ]]; then
#echo $server "&" $test "&" "&" "\\\\" >> $LATEX_TABLE_FILE
#elif [[ $CASE == "case6" ]]; then
#echo $server "&" $test "&" "&" "\\\\" >> $LATEX_TABLE_FILE
#fi
done
echo "\end{tabular}" >> $LATEX_TABLE_FILE
echo "\caption{$TEST}" >> $LATEX_TABLE_FILE
echo "\label{tab:$TEST}" >> $LATEX_TABLE_FILE
echo "\end{table}" >> $LATEX_TABLE_FILE
echo "Done."
cp $LATEX_TABLE_FILE /Users/hashkash-imac/Documents/HUB/my_work/QUIC/Papers/5f22f45a8109fa0001d2e213/Tables/
