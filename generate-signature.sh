#!/bin/bash
WD=./
DATADIR=../data/
ANALYSISDIR=../analysis/
DATATIMESTAMP=$1
cd $ANALYSISDIR/$DATATIMESTAMP

dot -Tsvg -Gcharset=utf8 -o $2.svg $2.dot
inkscape --export-text-to-path --export-area-drawing --export-eps=$2.eps $2.svg

