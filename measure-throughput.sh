#!/usr/bin/env bash
LSQUIC_PATH="/root/QUIC/Code/lsquic/build/"
TX_PATH="/root/QUIC/Experiments/data/tx/"
RX_PATH="/root/QUIC/Experiments/data/rx/"
SEND_VALUE=$1

if [[ $SEND_VALUE == "0" ]]; then

# TX SEND 0
TX_0_START=0
TX_0_END=0
#RX GET 0
ts=$(cat ${RX_PATH}rx-2.out | head -n1 | awk {'print $2'})
RX_0_START=$(date -d $ts +%s%N)
ts=$(cat ${RX_PATH}/rx-2.out | grep "conn: received HANDSHAKE_DONE frame" | awk {'print $2'})
RX_0_END=$(date -d $ts +%s%N)
RX_0_TIME=$(expr $RX_0_END - $RX_0_START)
echo "RX_0_TIME="$RX_0_TIME"ns"

else

# TX SEND 1
ts=$(cat ${TX_PATH}tx-2.out | head -n1 | awk {'print $2'})
TX_1_START=$(date -d $ts +%s%N)
ts=$(cat ${TX_PATH}/tx-2.out | grep "handshake: handshake has been confirmed" | awk {'print $2'})
TX_1_END=$(date -d $ts +%s%N)
TX_1_TIME=$(expr $TX_1_END - $TX_1_START)
echo "TX_1_TIME="$TX_1_TIME"ns"
#RX GET 1
ts=$(cat ${RX_PATH}rx-2.out | head -n1 | awk {'print $2'})
RX_1_START=$(date -d $ts +%s%N)
ts=$(cat ${RX_PATH}/rx-2.out | tail -n1 | awk {'print $2'})
RX_1_END=$(date -d $ts +%s%N)
RX_1_TIME=$(expr $RX_1_END - $RX_1_START)
echo "RX_1_TIME="$RX_1_TIME"ns"

fi
