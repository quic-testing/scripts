#!/bin/bash
WD=./
DATADIR=../data/
ANALYSISDIR=../analysis/
DATATIMESTAMP=$1
PORT=4433

cd $DATADIR/$DATATIMESTAMP

for case in case5 case6
do
echo $case
#for server in "quicly" "aioquic" "quant" "cloudflare" "ngtcp2" "nginx" "mvfst" "pquic" "neqo" "picoquic" "lsquic"
for server in "f5" "quinn" "akamai" "ats" "chromium"
do

echo $server;
pwd;
ls $case/$server;

FINGERPRINT_FILE=$case-$server-fingerprint-$DATATIMESTAMP.out
echo $FINGERPRINT_FILE
#echo "$case-$server, " > $FINGERPRINT_FILE
scid_file=$case/$server/$case*scid*.out
case_pcap_file=$case/$server/$case*.pcap

if [[ $server == "akamai" || $server == "quinn" ]]; then PORT=443; fi

is_http=0
result=$(echo $scid_file | grep -c "http")
if [[ $result == "1" ]]; then
is_http=1
fi

timestamp=$(ls $case/$server/$case*pcap | cut -d "-" -f 4-8 | cut -d "." -f 1)
echo "TIMESTAMP="$timestamp
if [[ $is_http == "0" ]]; then
case_pcap_file=$case/$server/$case-lsquic-$server-$timestamp.pcap
else
case_pcap_file=$case/$server/$case-lsquic_http-$server-$timestamp.pcap
fi

is_conn_close_client=0
echo "TEST FOR CONNECTION_CLOSE IN THE PCAP"
result=$(tshark -r $case_pcap_file -V | grep "CONNECTION_CLOSE (Transport) Error code:" -c)
if [[ $result -gt "0" ]]; then
is_conn_close_pcap=$(tshark -r $case_pcap_file -o tls.keylog_file:$keys_file -V | grep "CONNECTION_CLOSE (Transport) Error code:");
fi
error_code=$(echo ${is_conn_close_pcap} | cut -d " " -f 5)
echo "ERROR_CODE=$error_code, " >> $FINGERPRINT_FILE;

first_server_ip_len=$(tcpdump -neK -# -r $case_pcap_file src port $PORT | head -n1 | awk {'print $10'} | cut -d ":" -f 1)
echo "FIRST_SERVER_IP_LEN=$first_server_ip_len, " >> $FINGERPRINT_FILE;

serverpackets=$(tshark -r $case_pcap_file -V | grep "Source Port: $PORT" -c)
echo "PACKETS_FROM_SERVER=$serverpackets, " >> $FINGERPRINT_FILE;

result=$(tshark -r $case_pcap_file -V | grep "Source Connection ID: " | cut -d " " -f 8,12,16,20 )
echo $result | tr " " "\n" > source.cids
result=$(grep "0200000000000000" -c source.cids)
if [[ $result == "0" ]]; then
is_cid_changed="1";
echo "IS_CID_CHANGED=1" >> $FINGERPRINT_FILE
fi
if [[ $result -gt "0" ]]; then
is_cid_changed=0;
echo "IS_CID_CHANGED=0" >> $FINGERPRINT_FILE
fi

result=$(tshark -r $case_pcap_file -V | grep "Source Port: $PORT" -A 22 | grep "Source Connection ID Length: " -m 1 | cut -d " " -f 9)
echo "CID_LENGTH="$result >> $FINGERPRINT_FILE

# TLS SERVER HELLO ANALYSIS
supported_versions=$(tshark -r $case_pcap_file -V | grep "TLSv1.3 Record Layer: Handshake Protocol: Server Hello" -A 30 | grep -e "Supported Version:" | cut -d ":" -f 2 | cut -d " " -f 2-3)
echo "SUPPORTED_VERSIONS="$supported_versions >> $FINGERPRINT_FILE

cipher_suite=$(tshark -r $case_pcap_file -V | grep "TLSv1.3 Record Layer: Handshake Protocol: Server Hello" -A 30 | grep "Cipher Suite" | cut -d ":" -f 2 | cut -d " " -f 2)
echo "CIPHER_SUITE="$cipher_suite >> $FINGERPRINT_FILE

compression_method=$(tshark -r $case_pcap_file -V | grep "TLSv1.3 Record Layer: Handshake Protocol: Server Hello" -A 30 | grep "Compression Method" | cut -d ":" -f 2 | cut -d " " -f 2)
echo "COMPRESSION_METHOD="$compression_method >> $FINGERPRINT_FILE

extension_length=$(tshark -r $case_pcap_file -V | grep "TLSv1.3 Record Layer: Handshake Protocol: Server Hello" -A 30 | grep "Extensions Length" | cut -d ":" -f 2 | cut -d " " -f 2)
echo "EXTENSION_LENGTH="$extension_length >> $FINGERPRINT_FILE

key_share_group=$(tshark -r $case_pcap_file -V | grep "TLSv1.3 Record Layer: Handshake Protocol: Server Hello" -A 30 | grep "Key Share Entry: Group" | cut -d ":" -f 3 | cut -d " " -f 2 | cut -d "," -f 1)
echo "KEY_SHARE_GROUP="$key_share_group >> $FINGERPRINT_FILE

key_exchange_length=$(tshark -r $case_pcap_file -V | grep "TLSv1.3 Record Layer: Handshake Protocol: Server Hello" -A 30 | grep "Key Share Entry: Group" | cut -d ":" -f 4 | cut -d " " -f 2)
echo "KEY_EXCHANGE_LENGTH="$key_exchange_length >> $FINGERPRINT_FILE

done
done

cat /dev/null > fingerprints.out
for i in case*fingerprint-$DATATIMESTAMP.out;
do
echo $i >> fingerprints.out
cat $i >> fingerprints.out
done

echo "Make the respective analysis directory"
mkdir -p ../../analysis/$DATATIMESTAMP/passive
echo "Move the fingerprint files to the analysis directory"
mv *fingerprint* ../../analysis/$DATATIMESTAMP/passive

