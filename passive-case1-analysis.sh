#!/usr/bin/env bash
WD=./
DATADIR=../data/
DATATIMESTAMP=$1
PORT=4433

cd $DATADIR/$DATATIMESTAMP

for case in "case1"
do
echo $case
#for server in "quicly" "aioquic" "quant" "cloudflare" "ngtcp2" "nginx" "mvfst" "pquic" "neqo" "picoquic" "lsquic"
for server in "f5" "quinn" "akamai" "ats" "chromium"
do
echo $server
pwd
ls $case/$server

FINGERPRINT_FILE=$case-$server-fingerprint-$DATATIMESTAMP.out
echo $FINGERPRINT_FILE
case1_1_scid_file=$case/$server/case1_1*scid*.out
case1_2_scid_file=$case/$server/case1_2*scid*.out

if [[ $server == "akamai" || $server == "quinn" ]]; then PORT=443; fi

is_http=0
result=$(echo $case1_2_scid_file | grep -c "http")
if [[ $result == "1" ]]; then
is_http=1
fi

if [[ $is_http == "0" ]]; then
# TODO: The out scid*out file for case1-4 needs to have _1 and _2
timestamp=$(ls $case/$server/case1_1*pcap | cut -d "-" -f 4-8 | cut -d "." -f 1)
case1_1_pcap_file=$case/$server/case1_1-lsquic-$server-$timestamp.pcap
timestamp=$(ls $case/$server/case1_2*pcap | cut -d "-" -f 4-8 | cut -d "." -f 1)
case1_2_pcap_file=$case/$server/case1_2-lsquic-$server-$timestamp.pcap
else
timestamp=$(ls $case/$server/case1_1*pcap | cut -d "-" -f 4-8 | cut -d "." -f 1)
case1_1_pcap_file=$case/$server/case1_1-lsquic_http-$server-$timestamp.pcap
timestamp=$(ls $case/$server/case1_2*pcap | cut -d "-" -f 4-8 | cut -d "." -f 1)
case1_2_pcap_file=$case/$server/case1_2-lsquic_http-$server-$timestamp.pcap
fi

echo $case1_1_pcap_file
echo $case1_2_pcap_file

echo "TEST FOR PACKETS FROM SERVER"
serverpackets=$(tshark -r $case1_2_pcap_file -V | grep "Source Port: $PORT" -c)
echo "CASE1_2_NO_PACKETS_FROM_SERVER=$serverpackets, " >> $FINGERPRINT_FILE

done
done
