"""
Created on Mon Oct 1 2020

@author: Kashyap Thimmaraju
@email: k.thimmaraju@informatik.hu-berlin.de
"""

from explib import *

from datetime import datetime
import pprint

logtimestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
imacinterface = "lo0"
quicvminterface = "enp0s3"
capture_interface = quicvminterface
server_interface = "lo0"
imacpath = "/Users/hashkash-imac/Documents/HUB/my_work/QUIC/"
quicvmpath = "/root/QUIC/"
scriptPath = quicvmpath + "Experiments/scripts/"
dut_server = cnx_quicvm
dut_client = cnx_quicvm

##### quic servers #####
# servers = ["lsquic", "quicly", "aioquic", "quant", "mvfst"]
# servers = ["lsquic", "quicly", "quant", "mvfst", "aioquic"]
# servers = ["lsquic", "quicly", "quant"]
# servers = ["aioquic"]
# servers = ["quant"]
# servers = ["quicly", "aioquic", "quant", "cloudflare", "ngtcp2", "nginx", "mvfst", "pquic", "neqo", "picoquic"]
# servers = ["mvfst", "pquic", "neqo", "picoquic"]
# servers = ["picoquic"]
# servers = ["msquic"]
# servers = ["mvfst"]
# servers = ["lsquic"]
# servers = ["f5", "haskell-quic", "msquic", "quinn", "akamai"]
# servers = ["f5", "quinn", "akamai", "ats", "chromium"]
# servers = ["google"]
servers = ["37.120.131.40", "5.134.119.194"]
servers = ["5.134.119.194"]

clients = ["lsquic", "lsquic_http"]
clients = ["lsquic"]

##### quic server commands #####
quicly_server = ""
lsquic_server = ""
aioquic_server = ""
quant_server = ""
mvfst_server = ""

QUIC_PATH = quicvmpath + "Code/"

QUICLY_SERVER_PATH = QUIC_PATH + "quicly/"
LSQUIC_SERVER_PATH = QUIC_PATH + "litespeed/lsquic/"
QUANT_SERVER_PATH = QUIC_PATH + "quant/Debug/"
MVFST_SERVER_PATH = QUIC_PATH + "mvfst/_build/build/quic/samples/"
AIOQUIC_SERVER_PATH = QUIC_PATH + "aioquic/"


LSQUIC_CLIENT_PATH = QUIC_PATH + "lsquic/build/"

CASE0 = "case0"
CASE1 = "case1"
CASE2 = "case2"
CASE3 = "case3"
CASE4 = "case4"
CASE1_1 = "case1_1"
CASE1_2 = "case1_2"
CASE2_1 = "case2_1"
CASE2_2 = "case2_2"
CASE3_1 = "case3_1"
CASE3_2 = "case3_2"
CASE4_1 = "case4_1"
CASE4_2 = "case4_2"
CASE5 = "case5"
CASE6 = "case6"
CASE7 = "case7"


def cleanup(clients):
    print "cleanup()"
    for client in clients:
        stopclient(client)
    stopPacketCapture(dut_client)
    print "cleanup done!"


def getserveraddress(server, field="ip"):
    print "getservercommand()"
    ip = ""
    port = ""
    if server == "f5":
        ip = "f5quic.com"
        port = "4433"
    elif server == "haskell-quic":
        ip = "mew.org"
        port = "4433"
    elif server == "msquic":
        ip = "quic.westus.cloudapp.azure.com"
        port = "4433"
    elif server == "quinn":
        ip = "h3.stammw.eu"
        port = "443"
    elif server == "akamai":
        ip = "ietf.akaquic.com"
        port = "443"
    elif server == "ats":
        ip = "quic.ogre.com"
        port = "4433"
    elif server == "chromium":
        ip = "quic.rocks"
        port = "4433"
    elif server == "google":
        ip = "google.com"
        port = "443"
    elif server == "37.120.131.40":
        ip = "37.120.131.40"
        port = "443"
    elif server == "5.134.119.194":
        ip = "5.134.119.194"
        port = "443"
    if field == "ip":
        return ip
    elif field == "port":
        return port

def getclientcommand(client, timestamp, datapath, case, ip, port):
    print "getclientcommand()"
    if client == "lsquic":
        clientpath = LSQUIC_CLIENT_PATH
        #servercommand = #"\'" + \

        if case == CASE0 or case == CASE1 or case == CASE1_1 or case == CASE1_2 or case == CASE3 or case == CASE4:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "echo_client_localhost_scid1_dcid2 -H " + \
                            ip + " -s " + port + " -G " + \
                            datapath + " -L " + \
                            "debug 2>" + datapath + \
                            case + "-" + client + "-scid1_dcid2-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE5:
            # "127.0.0.1 -s 4433 -G " + \
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "echo_client_localhost_scid1_dcid3_newscid1 -H " + \
                            ip + " -s " + port + " -G " + \
                            datapath + " -L " +\
                            "debug 2>" + datapath + \
                            case + "-" + client + "-scid1_dcid3_newscid1-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE6:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "echo_client_localhost_scid1_dcid5_newscid6 -H " + \
                            ip + " -s " + port + " -G " + \
                            datapath + " -L " +\
                            "debug 2>" + datapath + \
                            case + "-" + client + "-scid1_dcid5_newscid6-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE7:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "echo_client_localhost_scid1_dcid4_newscid4 -H " + \
                            ip + " -s " + port + " -G " + \
                            datapath + " -L " +\
                            "debug 2>" + datapath + \
                            case + "-" + client + "-scid1_dcid4_newscid4-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
    elif client == "lsquic_http":
        clientpath = LSQUIC_CLIENT_PATH
        # ./http_client -4 -H 172.17.0.2 -s 4433 -G ./quic-keys/  -p / -M HEAD
        if case == CASE0 or case == CASE1 or case == CASE1_1 or case == CASE1_2 or case == CASE3 or case == CASE4:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "http_client_localhost_scid1_dcid2 -H " + \
                            ip + " -s " + port + " -G " + \
                            datapath + " -L debug " + \
                            "-M HEAD -p / " + \
                            "2&>" + datapath + \
                            case + "-" + client + "-scid1_dcid2-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE5:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "http_client_localhost_scid1_dcid3_newscid1 -H " + \
                            ip + " -s " + port + " -G " + \
                            datapath + " -L debug " + \
                            "-M HEAD -p / " + \
                            "2&>" + datapath + \
                            case + "-" + client + "-scid1_dcid3_newscid1-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE6:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "http_client_localhost_scid1_dcid5_newscid6 -H " + \
                            ip + " -s " + port + " -G " + \
                            datapath + " -L debug " + \
                            "-M HEAD -p / " + \
                            "2&>" + datapath + \
                            case + "-" + client + "-scid1_dcid5_newscid6-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE7:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "http_client_localhost_scid1_dcid4_newscid4 -H " + \
                            ip + " -s " + port + " -G " + \
                            datapath + " -L debug " + \
                            "-M HEAD -p / " + \
                            "2&>" + datapath + \
                            case + "-" + client + "-scid1_dcid4_newscid4-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand


def startclient(client, timestamp, datapath, case, ip, port):
    print "startclient()"
    clientcommand = getclientcommand(client, timestamp, datapath, case, ip, port)
    print "Going to start client on " + dut_client[0]
    clientcommand = "screen -dmSL clientSession " + clientcommand
    ssh = SSHConnect(cnx=dut_client)
    [output, error] = RunCommand(ssh, clientcommand)
    ssh.close()
    return error



def stopclient(client):
    print "stopclient()"
    print "Going to stop client: " + client
    ssh = SSHConnect(cnx=dut_client)
    # RunCommand(ssh, "screen -ls | grep Detached | "
    #                 "grep " + server + "|"
    #                 "cut -d . -f1 | awk '{print $1}' |"
    #                 "xargs kill")
    if client == "lsquic":
        # Need to stop the client twice if the echo connection is successful.
        RunCommand(ssh, "pkill echo_client")
        # RunCommand(ssh, "screen -S clientSession -X stuff $'\003'")
    elif client == "lsquic_http":
        RunCommand(ssh, "pkill http_client")
        # RunCommand(ssh, "screen -S clientSession -X stuff $'\003'")
    print "client stopped successfully"
    ssh.close()


def saveclientkeys(client, server, timestamp, datapath, case):
    print "saveclientkeys()"
    print "Going to save the client's key."
    ssh = SSHConnect(cnx=dut_client)
    RunCommand(ssh, "mv " + datapath + "0100000000000000.keys" + " " +
               datapath + case + "-" + client + "-" + server + "-" + timestamp + ".keys")
    ssh.close()


def runtests(testcases, server):
    print "runtests()"
    result = dict.fromkeys(range(0, 5))
    for testcase in testcases:
        if testcase == "case1":
            result[0] = case1(server)
        elif testcase == "case2":
            case2(server)
        elif testcase == "case3":
            case3(server)
        elif testcase == "case4":
            case4(server)
        elif testcase == "case5":
            case5(server)
        elif testcase == "case6":
            case6(server)
    return result

def case0(client, server):
    print "case0"
    '''
    Case 0
    The Client connects with scid=1 and dcid=2
    and attempts to send "hello" via a QUIC stream
    and then closes the connection.
    '''

    #### Run the appropriate server from the below list
    '''
    The server list could change over time
    '''
    print "**** Clean up everything before starting ****"
    cleanup(clients)
    print "\n\n"
    print "**** CASE 0 START ****"
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    datapath = prepareDataPath(dut_client, logtimestamp, CASE0, server)

    # First start the server
    # ip = "www.google.com"
    # port = "443"
    ip = getserveraddress(server, "ip")
    port = getserveraddress(server, "port")
    print "**** CONNECT TO SERVER: " + server + " ****\n\n"
    # exit()

    # Then start packet capture
    startPacketCapture(dut_client, capture_interface,
                       CASE0 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port " + str(port) + " or icmp\'"
                       )
    print "**** STARTED PACKET CAPTURE *****\n\n"

    print "Sleep 2s before starting..."
    time.sleep(2)

    # Start the client
    print "Now to start the client"
    # timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startclient(client, timestamp, datapath, CASE0, ip, port)
    print "**** STARTED THE CLIENT'S FIRST ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client and then start it again.
    stopclient(client)
    saveclientkeys(client, server, timestamp, datapath, CASE0)
    stopPacketCapture(dut_client)

    print "**** STOPPED THE CLIENT, SAVED THE KEYS, STOPPED THE SERVER AND PACKET CAPTURE ****\n\n"
    print "**** CASE 0 END ****"

def case1(client, server):
    print "case1"
    '''
    Case 1
    1.1. The Client connects with scid=1 and dcid=2
    and attempts to send "hello" via a QUIC stream
    and then closes the connection.
    1.2. The client connects with scid=1 and dcid=2
    and attempts to send "hello" and then closes
    the connection.
    '''

    #### Run the appropriate server from the below list
    '''
    The server list could change over time
    '''
    print "**** Clean up everything before starting ****"
    cleanup(clients)
    print "\n\n"
    print "**** CASE 1 START ****"
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    datapath = prepareDataPath(dut_client, logtimestamp, CASE1, server)

    # First start the server
    ip = getserveraddress(server, "ip")
    port = getserveraddress(server, "port")
    print "**** CONNECT TO SERVER: " + server + " ****\n\n"
    # exit()

    # Then start packet capture
    startPacketCapture(dut_client, capture_interface,
                       CASE1_1 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port " + str(port) + " or icmp\'"
                       )
    print "**** STARTED PACKET CAPTURE *****\n\n"

    print "Sleep 2s before starting..."
    time.sleep(2)

    # Start the client
    print "Now to start the client"
    # timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startclient(client, timestamp, datapath, CASE1_1, ip, port)
    print "**** STARTED THE CLIENT'S FIRST ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client and then start it again.
    stopclient(client)
    saveclientkeys(client, server, timestamp, datapath, CASE1_1)
    stopPacketCapture(dut_client)

    print "Sleep 10s and then start the client again..."
    time.sleep(10)

    print "Start the packet capture again..."
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startPacketCapture(dut_client, capture_interface,
                       CASE1_2 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port " + str(port) + " or icmp\'"
                       )
    print "Sleep 2s and then start the client..."
    time.sleep(2)
    startclient(client, timestamp, datapath, CASE1_2, ip, port)
    print "**** STARTED THE CLIENT'S SECOND ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client, server and packet capture .
    stopclient(client)
    saveclientkeys(client, server, timestamp, datapath, CASE1_2)
    # stopserver(server)

    time.sleep(2)
    stopPacketCapture(dut_client)
    print "**** STOPPED THE CLIENT, SAVED THE KEYS, STOPPED THE SERVER AND PACKET CAPTURE ****\n\n"
    print "**** CASE 1 END ****"


def case2():
    print "case2"
    '''
    Case 2
    1. The Client connects with scid=1 and dcid=1
    and attempts to send "hello" and then closes
    the connection.
    2. The client connects with scid=1 and dcid=1
    and attempts to send "hello" and then closes
    the connection.
    '''


def case3():
    print "case3"
    '''
    Case 3
    1. The Client connects with scid=1 and dcid=2
    and attempts to send "hello" and then closes
    the connection.
    2. The client connects with scid=1 and dcid=3
    and attempts to send "hello" and then closes
    the connection.
    '''



def case4():
    print "case4"
    '''
    Case 4
    1. The Client connects with scid=1 and dcid=2
    and attempts to send "hello" and then closes
    the connection.
    2. The client connects with scid=3 and dcid=2
    and attempts to send "hello" and then closes
    the connection.
    '''



def case5(client, server):
    print "case5"
    '''
    Case 5
    1. The Client connects with scid=1 and dcid=2
    and then issues $new_connection_ids$ with cids
    that are *all the same* as the scid (scid=1)
    and then attempts to send "hello" and then
    closes the connection.
    '''
    print "**** Clean up everything before starting ****"
    cleanup(clients)
    print "\n\n"
    print "**** CASE 5 START ****"
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    datapath = prepareDataPath(dut_client, logtimestamp, CASE5, server)

    ip = getserveraddress(server, "ip")
    port = getserveraddress(server, "port")
    print "**** CONNECT TO SERVER: " + server + " ****\n\n"

    # Then start packet capture
    startPacketCapture(dut_client, capture_interface,
                       CASE5 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port " + str(port) + " or icmp\'"
                       )
    print "**** STARTED PACKET CAPTURE *****\n\n"

    print "Sleep 2s before starting..."
    time.sleep(2)

    # Start the client
    print "Now to start the client"
    # timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startclient(client, timestamp, datapath, CASE5, ip, port)
    print "**** STARTED THE CLIENT'S FIRST ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client, server and packet capture .
    stopclient(client)
    saveclientkeys(client, server, timestamp, datapath, CASE5)

    time.sleep(2)
    stopPacketCapture(dut_client)
    print "**** STOPPED THE CLIENT, SAVED THE KEYS, STOPPED THE SERVER AND PACKET CAPTURE ****\n\n"
    print "**** CASE 5 END ****"


def case6(client, server):
    print "case6"
    '''
    Case 6
    1. The Client connects with scid=1 and dcid=2
    and then issues $new_connection_ids$ with cids
    that are *all the same* but different from the
    initial scid and then attempts to send "hello"
    and then closes the connection.
    '''
    print "**** Clean up everything before starting ****"
    cleanup(clients)
    print "\n\n"
    print "**** CASE 6 START ****"
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    datapath = prepareDataPath(dut_client, logtimestamp, CASE6, server)

    # First start the server
    ip = getserveraddress(server, "ip")
    port = getserveraddress(server, "port")
    print "**** CONNECT TO SERVER: " + server + " ****\n\n"

    # Then start packet capture
    startPacketCapture(dut_client, capture_interface,
                       CASE6 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port " + str(port) + " or icmp\'"
                       )
    print "**** STARTED PACKET CAPTURE *****\n\n"

    print "Sleep 2s before starting..."
    time.sleep(2)

    # Start the client
    print "Now to start the client"
    # timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startclient(client, timestamp, datapath, CASE6, ip, port)
    print "**** STARTED THE CLIENT'S FIRST ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client, server and packet capture .
    stopclient(client)
    saveclientkeys(client, server, timestamp, datapath, CASE6)

    time.sleep(2)
    stopPacketCapture(dut_client)
    print "**** STOPPED THE CLIENT, SAVED THE KEYS, STOPPED THE SERVER AND PACKET CAPTURE ****\n\n"
    print "**** CASE 6 END ****"


def main():
    print "main()"
    print "Script started at: " + str(logtimestamp)

    # cleanup(servers, clients)

    logs(logtimestamp)

    for server in servers:
        if server == "37.120.131.40" or server == "aioquic" or server == "nginx" or server == "msquic" or server == "mvfst"\
                or server == "pquic" or server == "picoquic" or server == "cloudflare"\
                or server == "f5" or server == "quinn" or server == "akamai"\
                or server == "chromium" or server == "google":
            client = "lsquic_http"
        elif server[0].isdigit() is True:
            client = "lsquic_http"
        else:
            client = "lsquic"
        case0(client, server)
        # case1(client, server)
        # case5(client, server)
        # case6(client, server)


def debug():
    print "debug()"
    #### Packet capture
    # startPacketCapture(dut, capture_interface, "foo.pcap",
    #                    scriptPath+"ExpLib_logs/"+logtimestamp+"/",
    #                    "micro",
    #                    "\'udp\'"
    #                    )
    # # time.sleep(1)
    # stopPacketCapture(dut)
    # cleanup(servers, clients)
    # startclient("lsquic_http", timestamp="now", datapath="/tmp/", case=CASE1)
    # stopserver("quant")
    # startserver("quant", timestamp="now", datapath="/tmp/", case=CASE5)
    # startclient("lsquic", timestamp="now", datapath="/tmp/", case=CASE5)
    # stopserver("quant")
    # ssh = SSHConnect(dut_server)
    # ssh.close()
    # for s in ["quant"]:
    # for s in servers:
    server = "37.120.131.40"
    # stopserver(server)
    # exit()
    # client = "lsquic_http"
    client = "lsquic_http"
    timestamp = "now" + server + client
    datapath = "/root/QUIC/Experiments/data/tmp/"
    # startserver(server, timestamp=timestamp, datapath=datapath, case=CASE5)
    startPacketCapture(dut_client, capture_interface,
                       CASE5 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port 4433 or icmp\'"
                       )
    # startclient(client, timestamp=timestamp, datapath=datapath, case=CASE0)
    # startclient(client, timestamp=timestamp, datapath=datapath, case=CASE0, ip, port)
    time.sleep(5)
    stopPacketCapture(dut_client)
    # stopserver(server)

if __name__ == "__main__":
    main()
    # debug()
