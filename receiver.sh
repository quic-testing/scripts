#!/usr/bin/env bash
LSQUIC_PATH="/root/QUIC/Code/lsquic/build/"
RX_PATH="/root/QUIC/Experiments/data/rx/"
RX_START_DELAY=0.005
RX_HOLD=0.023
TIMESTAMP_STYLE=5
SERVER_IP=127.0.0.1

mkdir -p $RX_PATH
cd $LSQUIC_PATH
result=$(ps aux | grep rx | grep echo)
if [[ $result -gt "0" ]]; then
ps aux | grep rx | grep echo | awk '{print $2}' | xargs kill -9
fi
for i in $(seq 2 12); do
#for i in 2; do
echo "RX_START_DELAY="$RX_START_DELAY
sleep $RX_START_DELAY
echo "run RX"$i
screen -dmSL rx-$i bash -c "taskset 03 ./echo_client_scid1_dcid$i -H $SERVER_IP -s 4433 -l sendctl=debug -l handshake=debug -G $RX_PATH -y $TIMESTAMP_STYLE 2> ${RX_PATH}rx-$i.out"
echo "RX_HOLD:"$RX_HOLD
sleep $RX_HOLD
done
for i in $(seq 2 12); do
#for i in 2; do
ps aux | grep "rx-"$i | grep echo | awk '{print $2}' | xargs kill -9
screen -wipe
done
