#!/usr/bin/env bash
TX_PATH="/root/QUIC/Experiments/data/tx/"
RX_PATH="/root/QUIC/Experiments/data/rx/"
SERVER_PATH="/root/QUIC/Experiments/data/server/"
echo "REMOVE .out FILES"
rm ${TX_PATH}*.out
rm ${RX_PATH}*.out
rm ${SERVER_PATH}*.out
