#!/usr/bin/env bash
LSQUIC_PATH="/root/QUIC/Code/lsquic/build/"
RX_PATH="/root/QUIC/Experiments/data/rx/"
RX_START_DELAY=0.005
RX_HOLD=0.023
IFD=0.030
TIMESTAMP_STYLE=5
SERVER_IP=127.0.0.1

mkdir -p $RX_PATH
cd $LSQUIC_PATH
result=$(ps aux | grep rx | grep echo)
if [[ $result -gt "0" ]]; then
ps aux | grep rx | grep echo | awk '{print $2}' | xargs kill -9
fi

FRAME_CID=2
DATA_BIT_CID=3

FRAME_COUNT=3
FRAME_SIZE=8

echo "RX_START_DELAY="$RX_START_DELAY
echo "RX_HOLD="$RX_HOLD
echo "IFD="$IFD

for f in $(seq 1 $FRAME_COUNT); do
echo "GET FRAME NUMBER:" $f
for c in $(seq 1 $FRAME_SIZE); do
if [[ $c == "1" ]]; then
echo "GET SOF BIT"
sleep $RX_START_DELAY
echo "run RX"$FRAME_CID
screen -dmSL rx-$FRAME_CID bash -c "taskset 03 ./echo_client_scid1_dcid$FRAME_CID -H $SERVER_IP -s 4433 -l sendctl=debug -l handshake=debug -G $RX_PATH -y $TIMESTAMP_STYLE 2> ${RX_PATH}rx-$FRAME_CID.out"
sleep $RX_HOLD
echo "Sent SOF:$FRAME_CID"
FRAME_CID=$(expr ${FRAME_CID} + 8)
# CONTINUE TO SEND THE DATA BITS
continue
else
echo "Get DATA_BIT_CID "$DATA_BIT_CID
sleep $RX_START_DELAY
echo "run RX"$DATA_BIT_CID
screen -dmSL rx-$DATA_BIT_CID bash -c "taskset 03 ./echo_client_scid1_dcid$DATA_BIT_CID -H $SERVER_IP -s 4433 -l sendctl=debug -l handshake=debug -G $RX_PATH -y $TIMESTAMP_STYLE 2> ${RX_PATH}rx-$DATA_BIT_CID.out"
sleep $RX_HOLD
DATA_BIT_CID=$(expr ${DATA_BIT_CID} + 1)
# DON'T USE THE SOF CIDS
if [[ $(expr ${DATA_BIT_CID} % 8) == "2" ]]; then DATA_BIT_CID=$(expr ${DATA_BIT_CID} + 1); fi
fi
done
sleep $IFD
done

for i in $(seq 2 ${DATA_BIT_CID}); do
ps aux | grep rx-$i | grep echo | awk '{print $2}' | xargs kill -9
screen -wipe >> rx.out
done