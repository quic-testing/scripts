#!/bin/bash
websitesfile=$1
rootpath='/Users/hashkash-imac/Documents/HUB/my_work/QUIC/'
chromiumpath=${rootpath}'Code/chrome-mac/Chromium.app/Contents/MacOS/'
datapath=${rootpath}'Experiments/data/Chromium/'
websitespath=${datapath}'quic-grabber_alexa-A/'
echo "Root path"
echo $rootpath
echo "Chromium path"
echo $chromiumpath
echo "Data path"
echo $datapath
echo "Websites path"
echo $websitespath
#chromium=${chromiumpath}'Chromium'
#echo $chromium
while read website; do
# reading each line
echo $website
mkdir ${datapath}${website}
timestamp=$(date "+%Y-%m-%d_%H:%M:%S")
#echo $timestamp
lognet='--log-net-log='${datapath}${website}'/chrome-netlog-'${timestamp}'.json'
echo 'lognet: '${lognet}
chromium=${chromiumpath}'/Chromium --headless '${lognet}' --net-log-capture-mode=IncludeSensitive --disable-gpu --enable-quic --origin-to-force-quic-on='${website}':443'
#chromium=${chromiumpath}'./Chromium --headless'
#echo $chromium
website='https://'${website}
$chromium $website
done < ${websitespath}${websitesfile}
