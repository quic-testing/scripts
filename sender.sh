#!/usr/bin/env bash
LSQUIC_PATH="/root/QUIC/Code/lsquic/build/"
TX_PATH="/root/QUIC/Experiments/data/tx/"
TX_START_DELAY=0.000
TX_HOLD=0.028
TIMESTAMP_STYLE=5
SERVER_IP=127.0.0.1

cd $LSQUIC_PATH
mkdir -p $TX_PATH
result=$(ps aux | grep tx | grep echo)
if [[ $result -gt "0" ]]; then
ps aux | grep tx | grep echo | awk '{print $2}' | xargs kill -9
fi
for i in $(seq 2 12); do
#for i in 2; do
echo "TX_START_DELAY="$TX_START_DELAY
sleep $TX_START_DELAY
echo "run TX"$i
screen -dmSL tx-$i bash -c "taskset 02 ./echo_client_scid1_dcid$i -H $SERVER_IP -s 4433 -l sendctl=debug -l handshake=debug -G $TX_PATH -y $TIMESTAMP_STYLE 2> ${TX_PATH}tx-$i.out"
echo "SLEEP:"$TX_HOLD" s"
sleep $TX_HOLD
echo "done"
done
for i in $(seq 2 12); do
#for i in 2; do
ps aux | grep tx-$i | grep echo | awk '{print $2}' | xargs kill -9
screen -wipe
done
