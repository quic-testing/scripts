#!/usr/bin/env bash
QUICLY_SERVER_PATH="/root/QUIC/Code/quicly/"
LSQUIC_PATH="/root/QUIC/Code/litespeed/lsquic/build/"
SERVER_PATH="/root/QUIC/Experiments/data/server/"
mkdir -p $SERVER_PATH

if [[ $1 == "lsquic" ]]; then
cd $LSQUIC_PATH
taskset 04 ./echo_server -s 127.0.0.1:4433 \
-c 127.0.0.1,$QUICLY_SERVER_PATH"t/assets/server.crt,"$QUICLY_SERVER_PATH"t/assets/server.key" 2&>> $SERVER_PATH"server.out"
elif [[ $1 == "ngtcp2" ]]; then
docker run -p4433:4433/udp ngtcp2-qfp
fi
