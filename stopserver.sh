#!/bin/bash
if [[ $1 == "lsquic" ]]; then
ps aux | grep "echo_server" | grep echo | awk '{print $2}' | xargs kill -9
elif [[ $1 == "ngtcp2" ]]; then
docker ps | grep "$1" | awk {'print $1'} | xargs docker stop
fi
