"""
Created on Wed Aug 19 2020

@author: Kashyap Thimmaraju
@email: k.thimmaraju@informatik.hu-berlin.de
"""

from explib import *

from datetime import datetime
import pprint

logtimestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
capture_interface = "lo0"
server_interface = "lo0"
scriptPath = "/Users/hashkash-imac/Documents/HUB/my_work/QUIC/Experiments/scripts/"
dut = cnx_localhost

##### quic servers #####
servers = ["lsquic", "quicly", "aioquic", "quant", "mvfst"]
servers = ["lsquic", "quicly", "quant", "mvfst", "aioquic"]
servers = ["lsquic", "quicly", "quant"]
servers = ["aioquic"]
# servers = ["quant"]

clients = ["lsquic", "lsquic_http"]
clients = ["lsquic"]

##### quic server commands #####
quicly_server = ""
lsquic_server = ""
aioquic_server = ""
quant_server = ""
mvfst_server = ""

QUIC_PATH = "/Users/hashkash-imac/Documents/HUB/my_work/QUIC/Code/"

QUICLY_SERVER_PATH = QUIC_PATH + "quicly/"
LSQUIC_SERVER_PATH = QUIC_PATH + "lsquic/"
QUANT_SERVER_PATH = QUIC_PATH + "quant/Debug/"
MVFST_SERVER_PATH = QUIC_PATH + "mvfst/_build/build/quic/samples/"
AIOQUIC_SERVER_PATH = QUIC_PATH + "aioquic/"


LSQUIC_CLIENT_PATH = QUIC_PATH + "lsquic/"


CASE1 = "case1"
CASE2 = "case2"
CASE3 = "case3"
CASE4 = "case4"
CASE5 = "case5"
CASE6 = "case6"
CASE7 = "case7"


def cleanup(servers, clients):
    print "cleanup()"
    for server in servers:
        stopserver(server)
    for client in clients:
        stopclient(client)
    stopPacketCapture(dut)
    print "cleanup done!"


def getservercommand(server, timestamp, datapath, case):
    print "getservercommand()"
    '''
    **** ngtcp2 docker image ****
    sudo docker run -p4433:4433/udp -t -i --entrypoint bash ngtcp2/ngtcp2-interop
    mkdir /www
    server 172.17.0.2 4433 /etc/ngtcp2/server.key /etc/ngtcp2/server.crt -s -d /www --qlog-dir /tmp
    
    
    '''
    # TODO: Consider running each server on a dedicated port instead of all of them using 4433
    if server == "quicly":
        serverpath = QUICLY_SERVER_PATH
        servercommand = "bash -c \'" +\
                        serverpath + \
                        "cli -v -c " +\
                        serverpath + "t/assets/server.crt -k " +\
                        serverpath + "t/assets/server.key -e " +\
                        datapath + case + "-" + server + "-server-events-" +\
                        timestamp + ".out " + \
                        "127.0.0.1 4433 2>" + \
                        datapath + case + "-" + server + "-server-" + \
                        timestamp + ".out" +\
                        "\'"
        return servercommand
    elif server == "lsquic":
        '''We use the quicly private keys and certs for lsquic also'''
        serverpath = LSQUIC_SERVER_PATH
        servercommand = "bash -c \'" + \
                        serverpath + \
                        "echo_server -s 127.0.0.1:4433 -L " \
                        "debug -y 1 -c " \
                        "127.0.0.1," +\
                        QUICLY_SERVER_PATH + "t/assets/server.crt," +\
                        QUICLY_SERVER_PATH + "t/assets/server.key 2>" +\
                        datapath + case + "-" + server + "-server-" + \
                        timestamp + ".out" +\
                        "\'"
        return servercommand
    elif server == "aioquic":
        serverpath = AIOQUIC_SERVER_PATH
        servercommand = "bash -c \'" + \
                        "/usr/local/bin/python3 " + \
                        serverpath +\
                        "examples/http3_server.py --host " \
                        "127.0.0.1 --port " \
                        "4433 --certificate " +\
                        QUICLY_SERVER_PATH + "t/assets/server.crt --private-key " + \
                        QUICLY_SERVER_PATH + "t/assets/server.key -v -q " +\
                        datapath + " 2>" + \
                        datapath + case + "-" + server + "-server-" + \
                        timestamp + ".out" +\
                        "\'"
        return servercommand
    elif server == "quant":
        serverpath = QUANT_SERVER_PATH
        servercommand = "bash -c \'" + \
                        serverpath + \
                        "bin/quant-server -q " +\
                        datapath + " -v 5 -c " +\
                        QUICLY_SERVER_PATH + "t/assets/server.crt -k " +\
                        QUICLY_SERVER_PATH + "t/assets/server.key 2>" +\
                        datapath + case + "-" + server + "-server-" +\
                        timestamp + ".out" +\
                        "\'"
        return servercommand
    elif server == "mvfst":
        '''
        when using the docker image lnicco/mvfst-qns
        sudo docker run -p4433:4433/udp -t -i --entrypoint bash lnicco/mvfst-qns
        /proxygen/proxygen/_build/proxygen/httpserver/hq --mode=server --port=4433 --static_root=/www --logdir=/tmp --qlogger_path=/tmp --host=172.17.0.2 --congestion=bbr --pacing=true --v=2
        '''
        serverpath = MVFST_SERVER_PATH
        servercommand = "bash -c \'" + \
                        serverpath + \
                        "echo -mode=server " \
                        "-host=127.0.0.1 " \
                        "-port=4433 -v 10 " \
                        "-logtostderr=false " \
                        "-log_dir=" +\
                        datapath +\
                        "\'"
        return servercommand


def startserver(server, timestamp="now", datapath="/tmp/", case="case"):
    print "startserver()"
    servercommand = getservercommand(server, timestamp, datapath, case)
    print "Going to start server: " + server + ", on " + dut[0] + " on interface:" + server_interface
    servercommand = "screen -dmSL serverSession " + servercommand
    ssh = SSHConnect(cnx=dut)
    [output, error] = RunCommand(ssh, servercommand)
    ssh.close()
    return error


def stopserver(server):
    print "stopserver()"
    print "Going to stop server: " + server
    ssh = SSHConnect(cnx=dut)
    # RunCommand(ssh, "screen -ls | grep Detached | "
    #                 "grep " + server + "|"
    #                 "cut -d . -f1 | awk '{print $1}' |"
    #                 "xargs kill")
    if server == "quicly":
        RunCommand(ssh, "pkill cli")
    elif server == "lsquic":
        RunCommand(ssh, "pkill echo_server")
    elif server == "aioquic":
        # TODO: Need a way to stop aioquic server. pkill does not work!
        # RunCommand(ssh, "screen -ls | grep Detached | "
        #             "grep serverSession " + " |"
        #             "cut -d . -f1 | awk '{print $1}' |"
        #             "xargs kill")
        RunCommand(ssh, "pkill python3")
    elif server == "mvfst":
        RunCommand(ssh, "pkill echo")
    elif server == "quant":
        RunCommand(ssh, "pkill quant-server")
    print "server stopped successfully"
    ssh.close()


def getclientcommand(client, timestamp, datapath, case, attempt=1):
    print "getclientcommand()"
    if client == "lsquic":
        clientpath = LSQUIC_CLIENT_PATH
        #servercommand = #"\'" + \

        # TODO: Need to toggle between the different echo clients for the respective cases.
        if (case == CASE1) or ((case == CASE3 or case == CASE4) and attempt == 1):
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "echo_client_localhost_scid1_dcid2 -H " + \
                            "127.0.0.1 -s 4433 -G " + \
                            datapath + " -L " + \
                            "debug 2>" + datapath + \
                            case + "-" + client + "-scid1_dcid2-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE5:
            # "127.0.0.1 -s 4433 -G " + \
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "echo_client_localhost_scid1_dcid2_newscid1 -H " + \
                            "127.0.0.1 -s 4433 -G " + \
                            datapath + " -L " +\
                            "debug 2>" + datapath + \
                            case + "-" + client + "-scid1_dcid2_newscid1-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE6:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "echo_client_localhost_scid1_dcid2_newscid3 -H " + \
                            "127.0.0.1 -s 4433 -G " + \
                            datapath + " -L " +\
                            "debug 2>" + datapath + \
                            case + "-" + client + "-scid1_dcid2_newscid3-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
    elif client == "lsquic_http":
        clientpath = LSQUIC_CLIENT_PATH
        # ./http_client -4 -H 127.0.0.1 -s 4433 -G ./quic-keys/  -p / -M HEAD
        if (case == CASE1) or ((case == CASE3 or case == CASE4) and attempt == 1):
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "http_client_localhost_scid1_dcid2 -H " + \
                            "127.0.0.1 -s 4433 -G " + \
                            datapath + " -p / -M HEAD -L " + \
                            "debug >" + datapath + \
                            case + "-" + client + "-scid1_dcid2-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE5:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "http_client_localhost_scid1_dcid2_newscid1 -H " + \
                            "127.0.0.1 -s 4433 -G " + \
                            datapath + " -p / -M HEAD -L " + \
                            "debug >" + datapath + \
                            case + "-" + client + "-scid1_dcid2_newscid1-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand
        elif case == CASE6:
            clientcommand = "bash -c \'" + \
                            clientpath + \
                            "http_client_localhost_scid1_dcid2_newscid3 -H " + \
                            "127.0.0.1 -s 4433 -G " + \
                            datapath + " -p / -M HEAD -L " + \
                            "debug >" + datapath + \
                            case + "-" + client + "-scid1_dcid2_newscid3-" + \
                            timestamp + ".out" + \
                            "\'"
            return clientcommand


def startclient(client, timestamp, datapath, case, attempt=1):
    print "startclient()"
    clientcommand = getclientcommand(client, timestamp, datapath, case, attempt)
    print "Going to start client on " + dut[0]
    clientcommand = "screen -dmSL clientSession " + clientcommand
    ssh = SSHConnect(cnx=dut)
    [output, error] = RunCommand(ssh, clientcommand)
    ssh.close()
    return error



def stopclient(client):
    print "stopclient()"
    print "Going to stop client: " + client
    ssh = SSHConnect(cnx=dut)
    # RunCommand(ssh, "screen -ls | grep Detached | "
    #                 "grep " + server + "|"
    #                 "cut -d . -f1 | awk '{print $1}' |"
    #                 "xargs kill")
    if client == "lsquic":
        # Need to stop the client twice if the echo connection is successful.
        RunCommand(ssh, "pkill echo_client")
        # RunCommand(ssh, "screen -S clientSession -X stuff $'\003'")
    elif client == "lsquic_http":
        RunCommand(ssh, "pkill http_client")
        # RunCommand(ssh, "screen -S clientSession -X stuff $'\003'")
    print "client stopped successfully"
    ssh.close()


def saveclientkeys(client, timestamp, datapath, case):
    print "saveclientkeys()"
    print "Going to save the client's key."
    ssh = SSHConnect(cnx=dut)
    RunCommand(ssh, "mv " + datapath + "0100000000000000.keys" + " " +
               datapath + case + "-" + client + "-" + timestamp + "-0100000000000000.keys")
    ssh.close()


def runtests(testcases, server):
    print "runtests()"
    result = dict.fromkeys(range(0, 5))
    for testcase in testcases:
        if testcase == "case1":
            result[0] = case1(server)
        elif testcase == "case2":
            case2(server)
        elif testcase == "case3":
            case3(server)
        elif testcase == "case4":
            case4(server)
        elif testcase == "case5":
            case5(server)
        elif testcase == "case6":
            case6(server)
    return result


def case1(client, server):
    print "case1"
    '''
    Case 1
    1.1. The Client connects with scid=1 and dcid=2
    and attempts to send "hello" via a QUIC stream
    and then closes the connection.
    1.2. The client connects with scid=1 and dcid=2
    and attempts to send "hello" and then closes
    the connection.
    '''

    #### Run the appropriate server from the below list
    '''
    The server list could change over time
    '''
    print "**** Clean up everything before starting ****"
    cleanup(servers, clients)
    print "\n\n"
    print "**** CASE 1 START ****"
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    datapath = prepareDataPath(dut, logtimestamp, CASE1, server)

    # First start the server
    startserver(server, timestamp, datapath, CASE1)
    print "**** STARTED THE SERVER *****\n\n"
    # exit()

    # Then start packet capture
    startPacketCapture(dut, capture_interface,
                       CASE1 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port 4433 or icmp\'"
                       )
    print "**** STARTED PACKET CAPTURE *****\n\n"

    print "Sleep 2s before starting..."
    time.sleep(2)

    # Start the client
    print "Now to start the client"
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startclient(client, timestamp, datapath, CASE1)
    print "**** STARTED THE CLIENT'S FIRST ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client and then start it again.
    stopclient(client)
    saveclientkeys(client, timestamp, datapath, CASE1)

    print "Sleep 10s and then start the client again..."
    time.sleep(10)

    print "Start the client again..."
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startclient(client, timestamp, datapath, CASE1)
    print "**** STARTED THE CLIENT'S SECOND ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client, server and packet capture .
    stopclient(client)
    saveclientkeys(client, timestamp, datapath, CASE1)
    stopserver(server)

    time.sleep(2)
    stopPacketCapture(dut)
    print "**** STOPPED THE CLIENT, SAVED THE KEYS, STOPPED THE SERVER AND PACKET CAPTURE ****\n\n"
    print "**** CASE 1 END ****"


def case2():
    print "case2"
    '''
    Case 2
    1. The Client connects with scid=1 and dcid=1
    and attempts to send "hello" and then closes
    the connection.
    2. The client connects with scid=1 and dcid=1
    and attempts to send "hello" and then closes
    the connection.
    '''


def case3():
    print "case3"
    '''
    Case 3
    1. The Client connects with scid=1 and dcid=2
    and attempts to send "hello" and then closes
    the connection.
    2. The client connects with scid=1 and dcid=3
    and attempts to send "hello" and then closes
    the connection.
    '''



def case4():
    print "case4"
    '''
    Case 4
    1. The Client connects with scid=1 and dcid=2
    and attempts to send "hello" and then closes
    the connection.
    2. The client connects with scid=3 and dcid=2
    and attempts to send "hello" and then closes
    the connection.
    '''



def case5(client, server):
    print "case5"
    '''
    Case 5
    1. The Client connects with scid=1 and dcid=2
    and then issues $new_connection_ids$ with cids
    that are *all the same* as the scid (scid=1)
    and then attempts to send "hello" and then
    closes the connection.
    '''
    print "**** Clean up everything before starting ****"
    cleanup(servers, clients)
    print "\n\n"
    print "**** CASE 5 START ****"
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    datapath = prepareDataPath(dut, logtimestamp, CASE5, server)

    # First start the server
    startserver(server, timestamp, datapath, CASE5)
    print "**** STARTED THE SERVER *****\n\n"

    # Then start packet capture
    startPacketCapture(dut, capture_interface,
                       CASE5 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port 4433 or icmp\'"
                       )
    print "**** STARTED PACKET CAPTURE *****\n\n"

    print "Sleep 2s before starting..."
    time.sleep(2)

    # Start the client
    print "Now to start the client"
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startclient(client, timestamp, datapath, CASE5)
    print "**** STARTED THE CLIENT'S FIRST ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client, server and packet capture .
    stopclient(client)
    saveclientkeys(client, timestamp, datapath, CASE5)
    stopserver(server)

    time.sleep(2)
    stopPacketCapture(dut)
    print "**** STOPPED THE CLIENT, SAVED THE KEYS, STOPPED THE SERVER AND PACKET CAPTURE ****\n\n"
    print "**** CASE 5 END ****"


def case6(client, server):
    print "case6"
    '''
    Case 6
    1. The Client connects with scid=1 and dcid=2
    and then issues $new_connection_ids$ with cids
    that are *all the same* but different from the
    initial scid and then attempts to send "hello"
    and then closes the connection.
    '''
    print "**** Clean up everything before starting ****"
    cleanup(servers, clients)
    print "\n\n"
    print "**** CASE 6 START ****"
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    datapath = prepareDataPath(dut, logtimestamp, CASE6, server)

    # First start the server
    startserver(server, timestamp, datapath, CASE6)
    print "**** STARTED THE SERVER *****\n\n"

    # Then start packet capture
    startPacketCapture(dut, capture_interface,
                       CASE6 + "-" + client + "-" + server + "-" + timestamp + ".pcap",
                       datapath,
                       "micro",
                       "\'udp port 4433 or icmp\'"
                       )
    print "**** STARTED PACKET CAPTURE *****\n\n"

    print "Sleep 2s before starting..."
    time.sleep(2)

    # Start the client
    print "Now to start the client"
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    startclient(client, timestamp, datapath, CASE6)
    print "**** STARTED THE CLIENT'S FIRST ATTEMPT *****\n\n"

    print "Sleep 2s and then stop the client..."
    time.sleep(2)

    # Stop the client, server and packet capture .
    stopclient(client)
    saveclientkeys(client, timestamp, datapath, CASE6)
    stopserver(server)

    time.sleep(2)
    stopPacketCapture(dut)
    print "**** STOPPED THE CLIENT, SAVED THE KEYS, STOPPED THE SERVER AND PACKET CAPTURE ****\n\n"
    print "**** CASE 6 END ****"


def main():
    print "main()"
    print "Script started at: " + str(logtimestamp)

    cleanup(servers, clients)

    logs(logtimestamp)

    for server in servers:
        if server == "aioquic":
            client = "lsquic_http"
        else:
            client = "lsquic"
        case1(client, server)
        case5(client, server)
        case6(client, server)


def debug():
    print "debug()"
    #### Packet capture
    # startPacketCapture(dut, capture_interface, "foo.pcap",
    #                    scriptPath+"ExpLib_logs/"+logtimestamp+"/",
    #                    "micro",
    #                    "\'udp\'"
    #                    )
    # # time.sleep(1)
    # stopPacketCapture(dut)
    # cleanup(servers, clients)
    startclient("lsquic", timestamp="now", datapath="/tmp/", case=CASE1)
    # startclient("lsquic_http", timestamp="now", datapath="/tmp/", case=CASE1)


if __name__ == "__main__":
    main()
    # debug()

