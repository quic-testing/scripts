#!/usr/bin/env bash
AIOQUIC=/root/QUIC/Code/aioquic/tests/
LSQUIC=/root/QUIC/Code/litespeed/lsquic/tests
MSQUIC=/root/QUIC/Code/msquic/src/test/lib
NEQO=/root/QUIC/Code/msquic/src/test/lib
# NEQO has tests within each folder
#root@kreuzberg:~/QUIC/Code/neqo# find . -name tests
#./neqo-http3/tests
#./neqo-crypto/tests
#./neqo-common/tests
#./neqo-transport/src/connection/tests
#./neqo-transport/tests
NGTCP=/root/QUIC/Code/ngtcp2/tests
PICOQUIC=/root/QUIC/Code/picoquic/picoquictest
PQUIC=/root/QUIC/Code/picoquic/picoquictest
QUICHE=/root/QUIC/Code/quiche
QUICLY=/root/QUIC/Code/quicly/t
MVFST=/root/QUIC/Code/mvfst/quic
# MVFST has tests within each folder
#root@kreuzberg:~/QUIC/Code/mvfst/quic# find . -name *test*
#./common/test
#./handshake/test
#./codec/test
#./client/test
#./congestion_control/test
#./d6d/test
#./api/test
#./tools/tperf/test
#./fizz/handshake/test
#./fizz/client/handshake/test
#./fizz/client/test
#./state/test
#./state/stream/test
#./loss/test
#./server/handshake/test
#./server/test
#./server/async_tran/test
#./flowcontrol/test
#./logging/test
NGINX=/root/QUIC/Code/nginx-quic/nginx-quic-3443ee341cc1/src/misc
#mkdir -p /root/QUIC/Experiments/data/tc-analysis
TCPATH=../data/tc-analysis
##if [[ $1 != "" ]]; then
##LOG=$TCPATH/tc-analysis-$1
##echo "USING USER-PROVIDED TIMESTAMP: "$LOG
##else

