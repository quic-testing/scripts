#!/usr/bin/env bash
LSQUIC_PATH="/root/QUIC/Code/lsquic/build/"
TX_PATH="/root/QUIC/Experiments/data/tx/"
TX_START_DELAY=0.000
TX_HOLD=0.028
IFD=0.030
TIMESTAMP_STYLE=5
SERVER_IP=127.0.0.1
ENCODED_MESSAGE="/root/QUIC/Experiments/scripts/message-encoded"

cd $LSQUIC_PATH
mkdir -p $TX_PATH
result=$(ps aux | grep tx | grep echo)
if [[ $result -gt "0" ]]; then
ps aux | grep tx | grep echo | awk '{print $2}' | xargs kill -9
fi

FRAME_CID=2
DATA_BIT_CID=3

echo "TX_START_DELAY="$TX_START_DELAY
echo "TX_HOLD="$TX_HOLD
echo "IFD="$IFD
while IFS= read -r frame; do
echo "SEND FRAME:"$frame
frame_len=$(expr ${#frame} - 1)
for c in `seq 0 $frame_len`; do
#echo "C:"$c
#echo ${frame:$c:1}
#continue
if [[ $c == "0" ]]; then
echo "SEND SOF BIT"
sleep $TX_START_DELAY
echo "run TX"$FRAME_CID
screen -dmSL tx-$FRAME_CID bash -c "taskset 02 ./echo_client_scid1_dcid$FRAME_CID -H $SERVER_IP -s 4433 -l sendctl=debug -l handshake=debug -G $TX_PATH -y $TIMESTAMP_STYLE 2> ${TX_PATH}tx-$FRAME_CID.out"
sleep $TX_HOLD
echo "Sent SOF:$FRAME_CID"
FRAME_CID=$(expr ${FRAME_CID} + 8)
continue
elif [[ $c != "0" && ${frame:$c:1} == "1" ]]; then
echo "SEND "${frame:$c:1}
sleep $TX_START_DELAY
echo "run TX"$DATA_BIT_CID
screen -dmSL tx-$DATA_BIT_CID bash -c "taskset 02 ./echo_client_scid1_dcid$DATA_BIT_CID -H $SERVER_IP -s 4433 -l sendctl=debug -l handshake=debug -G $TX_PATH -y $TIMESTAMP_STYLE 2> ${TX_PATH}tx-$DATA_BIT_CID.out"
sleep $TX_HOLD
elif [[ $c != "0" && ${frame:$c:1} == "0" ]]; then
echo "SEND "${frame:$c:1}
echo "CID "$DATA_BIT_CID" for 0"
sleep $TX_START_DELAY
sleep $TX_HOLD
fi
DATA_BIT_CID=$(expr ${DATA_BIT_CID} + 1)
# DON'T USE THE SOF CIDS
if [[ $(expr ${DATA_BIT_CID} % 8) == "2" ]]; then
DATA_BIT_CID=$(expr ${DATA_BIT_CID} + 1)
fi

done
sleep $IFD
done < $ENCODED_MESSAGE

for i in $(seq 2 ${DATA_BIT_CID}); do
ps aux | grep tx-$i | grep echo | awk '{print $2}' | xargs kill -9
screen -wipe >> tx.out
done
